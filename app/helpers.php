<?php
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

function buildUrlQuery($url, array $query)
{
    // append the query string
    if (!empty($query)) {
        $queryString = http_build_query($query);
        $url .= '?'.$queryString;
    }

    return $url;
}

function appendUrlQuery(array $query)
{
  $currentUrl = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
  $currentUrlPart = parse_url($currentUrl);
  $url = '';
  if (!empty($query)) {
    if(!empty($currentUrlPart['query'])) {
      parse_str($currentUrlPart['query'], $queries);
      $queries = array_merge($queries, $query);
      $queryString = http_build_query($queries);
    } else {
      $queryString = http_build_query($query);
    }
    $url = "{$currentUrlPart['scheme']}://{$_SERVER['HTTP_HOST']}{$currentUrlPart['path']}?{$queryString}";
  } else {
    $url = $currentUrl;
  }
  return $url;
}

function backendUrl($url)
{
    return url(Config::get('backend.url').'/'.$url);
}

function localizedUrl($uri)
{
    return LaravelLocalization::localizeURL($uri);
}

function saveImage($file, $path = null) {

    if(gettype($file) != 'string' && get_class($file) == \Symfony\Component\HttpFoundation\File\UploadedFile::class) {
      $imageResource = file_get_contents($file->getRealPath());
    } else {
      $imageResource = $file;
    }

    $imageToSave = (string) Image::make($imageResource)->resize(600, null , function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
    })->encode('png',80);

    $imageName = randomString().'.png';
    if(!empty($path)) {
        $imageName = $path . $imageName;
    }

    $imageName = ltrim($imageName,'/');

    switch (App::environment()) {
        case 'production':
            // Storage::disk('s3')->put($imageName, $imageToSave);
            // return "https://s3-ap-northeast-1.amazonaws.com/promoec/{$imageName}";

            Storage::disk('local')->put($imageName, $imageToSave);
            $cloudder = Cloudder::upload(storage_path('app/'.$imageName),null,[],$path);
            return Cloudder::secureShow($cloudder->getPublicId(),[
              'width' => 600,
              'height' => 600,
            ]);

            break;
        default:
            Storage::disk('local')->put($imageName, $imageToSave);

            // $cloudder = Cloudder::upload(storage_path('app/'.$imageName),null,[],$path);
            // return Cloudder::show($cloudder->getPublicId(),[
            //   'width' => 600,
            //   'height' => 600,
            // ]);

            $url = url("api/image/?path={$imageName}");
            if(App::runningInConsole()) {
              $url = Config::get('app.url') . "/api/image/?path=/{$imageName}";
            }

            return $url;
            break;
    }
}

function randomString() {
    return md5(rand(100, 200));
}

function getStorePreviewToken($user, $store) {
  return sha1($user->id.$store->id.'store_preview');
}

function firebaseRegister($companyId) {
  $serviceAccount = ServiceAccount::fromJsonFile(base_path('doc/ecjo-1487813611204-firebase-adminsdk-qzo1l-025858c39f.json'));
  $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->withDatabaseUri('https://ecjo-1487813611204.firebaseio.com')
    ->create();

  ///per company, not per user...

  $customToken = $firebase->getAuth()->createCustomToken("ecjo-company-" . $companyId);
  return $customToken;
}
