<?php

namespace App\Utilities;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
  protected $fillable = [
    "key",
    "currency_locale",
    "country",
    "name",
    "fetchUrl",
    "dummyFile",
    "affiliateId",
    "productUrl",
    "currency",
  ];
}
