<?php

/**
 *
 */

namespace App\Utilities;

use JsonSerializable;

/**
 *
 * @link http://labs.omniti.com/labs/jsend
 */
final class JSend implements JsonSerializable
{

    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';
    const STATUS_ERROR = 'error';

    protected $status;
    protected $data;
    protected $message;
    protected $code;

    private function __construct($status)
    {
        $this->status = $status;
    }

    /**
     *
     * @param string|array $data
     * @return static
     */
    public static function success($data, $message = '')
    {
        $success = new static(static::STATUS_SUCCESS);
        $success->data = $data;
        $success->message = $message;
        return $success;
    }

    /**
     *
     * @param string|array $data
     * @return static
     */
    public static function fail($data)
    {
        $fail = new static(static::STATUS_FAIL);
        $fail->data = $data;
        return $fail;
    }

    /**
     *
     * @param string $message
     * @return static
     */
    public static function error($message)
    {
        $error = new static(static::STATUS_ERROR);
        $error->message = $message;
        return $error;
    }

    public function setCode($code)
    {
        if (static::STATUS_ERROR === $this->status) {
            $this->code = $code;
        } else {
            throw new \Exception("code can only be set if status = error");
        }
    }

    public function setData($data)
    {
        if (static::STATUS_ERROR === $this->status) {
            $this->data = $data;
        } else {
            throw new \Exception("data can only be set if status = error");
        }
    }

    public function jsonSerialize()
    {
        $json = array('status' => $this->status,);
        if (isset($this->data)) {
            $json['data'] = $this->data;
        }
        if (isset($this->message)) {
            $json['message'] = $this->message;
        }
        if (isset($this->code)) {
            $json['code'] = $this->code;
        }
        return $json;
    }

}
