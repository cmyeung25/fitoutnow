<?php

/**
 * Put file document here
 */

namespace App\Utilities\Traits;

use Closure;
use Container;
use Request;
use Symfony\Component\HttpFoundation\Response;
use App\Utilities\JSend;

/**
 * Put class document here
 *
 */
trait TraitRestFromValidationController
{

    private static $validStatusCode = array(
        "OK" => 200,
        "CREATED" => 201,
        "NO_CONTENT" => 204,
        "NOT_MODIFIED" => 304,
        "BAD_REQUEST" => 400,
        "FORBIDDEN" => 403,
        "NOT_FOUND" => 404,
        "METHOD_NOT_ALLOWED" => 405,
        "GONE" => 410,
        "UNSUPPORTED_MEDIA_TYPE" => 415,
        "UNPROCESSABLE_ENTITY" => 422,
        "TOO_MANY_REQUESTS" => 429,
        "INTERNAL_SERVER_ERROR" => 500
    );

// <editor-fold defaultstate="collapsed" desc="Up and down">

    /**
     *
     * @return Response
     */
    private static function getResponse()
    {
        $response = new Response;
        return $response;
    }

    private static function send(Response $response, $jsend)
    {
        $response->setContent(json_encode($jsend));
        // $response->send();

        return $response;
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Low level">

    protected static function checkIsStatusCodeValid($statusCode)
    {
        if (! in_array($statusCode, array_values(static::$validStatusCode))) {
            throw new \Exception(__CLASS__ . '::' . __FILE__ . " status code {$statusCode} is not allowed.");
        }
    }

    protected static function fail($data, $statusCode = Response::HTTP_BAD_REQUEST)
    {
        static::checkIsStatusCodeValid($statusCode);

        $response = static::getResponse();
        $response->setStatusCode($statusCode);
        $jsend = JSend::fail($data);
        return static::send($response, $jsend);
    }

    protected static function success($data, $message = '', $statusCode = Response::HTTP_OK)
    {
        static::checkIsStatusCodeValid($statusCode);

        $response = static::getResponse();
        $response->setStatusCode($statusCode);
        $jsend = JSend::success($data, $message);
        return static::send($response, $jsend);
    }

    protected static function error($message, $statusCode = Response::HTTP_BAD_REQUEST)
    {
        static::checkIsStatusCodeValid($statusCode);

        $response = static::getResponse();
        $response->setStatusCode($statusCode);
        $jsend = JSend::error($message);
        return static::send($response, $jsend);
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Shortcuts">

    /**
     *
     * @param string $url
     */
    public static function failWithRedirect($url)
    {
        return static::fail(array('redirectUrl' => $url));
    }

    public static function failWithMessage($message, $callback = '')
    {
        return static::fail(array(
            'message' => $message,
            'callback' => $callback,
        ));
    }

    /**
     *
     * @param array[]|array $validationMessageArray
     */
    public static function failValidation($validationMessageArray, $callback = '')
    {
        return static::fail(array(
            'validator' => $validationMessageArray,
            'callback' => $callback,
        ));
    }

    /**
     *
     * @param string $url
     * @param string $notice
     */
    public static function successWithRedirect($url, $notice = null)
    {

        if(!empty($notice)){
            Request::session()->flash('flash_message', $notice);
        }
        return static::success(array(
            'redirectUrl' => $url,
            'notice' => $notice,
        ));
    }


    /**
     *
     * @param string $message
     */
    public static function errorWithMessage($message)
    {
        return static::error($message);
    }

    /**
     * Send message with 400 (BAD_REQUEST)
     *
     * @param string $message
     */
    public static function errorBadRequest($message)
    {
        return static::error($message, Response::HTTP_BAD_REQUEST);
    }

// </editor-fold>

}
