<?php
namespace App\Utilities\Traits;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use LaravelLocalization;
use DB;
use Validator;
use Config;
use View;

use App\Utilities\Traits\TraitRestFromValidationController;
use App\Utilities\Widgets\View\Widgets\DataTable\DataTable;
use App\Utilities\Widgets\View\Widgets\Paginator\Paginator;


use Exception;
use Closure;
/**
 * @property $model 'e.g. User::class', The model that binded with the controller with base crud operation
 * @property $viewPath 'e.g. frontend.member', The path of storaging the view in the resources folder.
 * @property $baseRoute 'e.g. member' => domain/{baseRoute}/detail, The route prefix that set in the route.php
 * @property $modelDataName 'e.g. users|default:data' , The variable that pass to the view from the controller.
 * @property $localeMode ['single','multi'], Single: One page with one locale to update; Multi: One page with multi locale to update.
 * @property $backendController boolean , true -> get backend url from config.
 * @todo $localeMode
 */

trait TraitBaseCrudController {

    use TraitRestFromValidationController;

    public function getIndex(Request $request)
    {
        return redirect($this->getBaseRoute().'/listing');
    }

    public function getDataTableColumns()
    {
        return [];
    }

    public function getListing(Request $request, $callback = null, $perPage = 10){
        $locale = LaravelLocalization::getCurrentLocale();

        if(!empty($request->locale))
            $locale = $request->locale;

        $model = $this->getModel();
        $query = $model::query();

        if(isset($callback)) {
            $callback($query, $locale);
        }

        if(in_array('App\Utilities\Traits\TraitLocalizableModel', class_uses($model))){
            $query->withLocale($locale);
        }

        $requestQuery = $request->query();
        if(isset($requestQuery['sort']) and isset($requestQuery['d'])) {
            $query->orderBy($requestQuery['sort'],$requestQuery['d']);
        }

        if(isset($requestQuery['keyword'])) {
            $keyword = $requestQuery['keyword'];
            if(method_exists($this, 'getKeywordSearchBuilder')) {
                $query = $this->getKeywordSearchBuilder($query, $keyword);
                View::share('keyword', $keyword);
            } else {
                throw new Exception("Please define getKeywordSearchBuilder to get the search result");
            }
        }

        $modelData = $query->paginate($perPage);
        $dataTable = new DataTable($modelData->items());
        $paginator = new Paginator($modelData);
        $dataTable->setColumns($this->getDataTableColumns());
        $dataTable->setActionBaseUrl(url($this->getBaseRoute()));

        if(method_exists($this, 'configListingDataTable')) {
            $this->configListingDataTable($dataTable);
        }


        return view("{$this->getViewPath()}.listing", [
            $this->getModelDataName() => $modelData,
            'locale' => $locale,
            'dataTable' => $dataTable->toArray(),
            'paginator' => $paginator->toArray(),
            'add_new_url' => url($this->getBaseRoute().'/detail/0'),
            'search_url' => url($this->getBaseRoute().'/listing'),
        ]);
    }

    public function getDetail(Request $request, $id) {
        $locale = !empty($request->locale) ? $request->locale : LaravelLocalization::getCurrentLocale();

        if(method_exists($this, 'onBeforeGetDetail')) {
            $this->onBeforeGetDetail($request, $id, $locale);
        }

        $model = $this->getModel();
        $query = $model::query();
        if(in_array('App\Utilities\Traits\TraitLocalizableModel', class_uses($model))){
            $query->withLocale($locale);
        }

        if(in_array('Conner\Tagging\Taggable', class_uses($model))) {
            $query->with('tagged');
            $tags = $model::existingTags();
            $tagOptions = [];
            foreach ($tags as $key => $tag) {
                $tagOptions[$tag['name']] = $tag['name'];
            }
            View::share('tags', $tagOptions);

        }

        $modelData = $query->find($id);
        if( $modelData == null and $id != 0) {
            throw new Exception("Missing Record", 1);
        }


        if(method_exists($this, 'onAfterGetDetail')) {
            $returnFromOnAfterGetDetail = $this->onAfterGetDetail($request, $id, $locale, $modelData);
            if($returnFromOnAfterGetDetail instanceof Response || $returnFromOnAfterGetDetail instanceof \Illuminate\View\View) {
                return $returnFromOnAfterGetDetail;
            }
        }

        $returnData = null;
        if(!empty($modelData)) {
            $returnData = $modelData->toArray();

            if(in_array('Conner\Tagging\Taggable', class_uses($model))) {
                $returnData['tags'] = [];
                foreach ($returnData['tagged'] as $tag) {
                    $returnData['tags'][] = $tag['tag_name'];
                }
            }
        }


        return view("{$this->getViewPath()}.detail", [
            $this->getModelDataName() => $returnData,
            'locales' => LaravelLocalization::getSupportedLocales(),
            'locale' => $locale,
            'form_action' => url($this->getBaseRoute().'/detail/'.$id),
        ]);
    }

    public function getPostDetailValidationRules()
    {
        return [];
    }

    public function getPostDetailValidationMessages()
    {
        return [];
    }

    public function postDetail(Request $request, $id)
    {
        DB::beginTransaction();
        $postData = $request->all();
        $validator = Validator::make($request->all(), $this->getPostDetailValidationRules(), $this->getPostDetailValidationMessages());
        if($validator->fails())
        {
            if($request->ajax()) {
                return static::failValidation($validator->errors()->toArray());
            }
            return back()->withErrors($validator)->withInput();
        }

        $postData = $this->clearEmptyPostData($postData);

        if(method_exists($this, 'preparePostData')) {
            $postData = array_merge($postData, $this->preparePostData($request, $id));
        }

        $model = $this->getModel();
        if($id == 0) {
            $instance = new $model;
            $instance = $instance->create($postData);
            $id = $instance[$instance->getKeyName()];
        } else {
            $instance = $model::find($id);
        }
        $instance->update($postData);

        if(in_array('Conner\Tagging\Taggable', class_uses($model))) {
            if(!empty($postData['tags'])) {
                $instance->retag($postData['tags']);
            }
        }

        if(method_exists($this, 'afterPostDetail')) {
            $this->afterPostDetail($request, $id, $instance);
        }

        DB::commit();

        $redirectUrl = $this->getPostDetailRedirectUrl($instance[$instance->getKeyName()], $postData['locale']);

        if($request->ajax()) {
            return static::successWithRedirect($redirectUrl);
        }

        return redirect($redirectUrl)->with('success', 'Success!');;

    }

    public function getDelete(Request $request, $id)
    {
        if(!is_array($id))
            $id = array($id);

        $model = $this->getModel();
        $instance = new $model;
        $model::whereIn($instance->getKeyName(), $id)->delete();
        return redirect($this->getBaseRoute().'/listing/');
    }

    //protected function

    protected function getModel() {
        if(empty($this->model))
            throw new Exception("Missing Model Name", 1);

        return $this->model;
    }

    protected function getViewPath() {
        if(empty($this->viewPath))
            throw new Exception("Missing View Path", 1);

        return $this->viewPath;
    }

    protected function getPostDetailRedirectUrl($id, $locale = null)
    {
        $redirectUrl = url($this->getBaseRoute().'/detail/'. $id);

        if(!empty($locale)) {
            $redirectUrl = $redirectUrl . '?locale=' . $locale;
        }

        return $redirectUrl;
    }

    protected function getModelDataName() {
        return !empty($this->modelDataName) ? $this->modelDataName : 'data';
    }

    protected function getBaseRoute() {
        if(empty($this->baseRoute))
            throw new Exception("Missing Base Route", 1);

        if(!empty($this->backendController) and $this->backendController == true) {
            $baseRoute = Config::get('backend.url'). $this->baseRoute;
        } else {
            $baseRoute = $this->baseRoute;
        }


        return $baseRoute;
    }

    protected function getCreateAndUpdateLocaleMode() {
        if(in_array('App\Utilities\Traits\TraitLocalizableModel', class_uses($this->getModel()))){
            return property_exists('localeMode') ? $this->localeMode : 'single';
        }
        else
        {
            return 'single';
        }
    }

    protected function clearEmptyPostData($postData) {
        foreach ($postData as $key => &$data) {
            if(empty($data)) {
                unset($postData[$key]);
            }
        }
        return $postData;
    }
}
