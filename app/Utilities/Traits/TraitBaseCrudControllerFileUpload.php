<?php
namespace App\Utilities\Traits;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;
use Image;
use Input;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait to handle file upload
 *
 * All field are paired with with a {field}_url to disclouse the url in public
 * e.g. photo[input::file] + photo_url[will be generated from the trait]
 *
 * @property $uploadPath default:'photo'
 * @callback onImageMaking($field, UploadedFile $file, Image $img)
 */

trait TraitBaseCrudControllerFileUpload {

    /**
     * @return array [] fields name for file upload fields
     */
    abstract public function getFileUploadFields();

    public function afterPostDetail(Request $request, $id, Model $instance) {
        //create
        $this->createUploadPath($id);
        $uploadData = [];

        foreach($this->getFileUploadFields() as $field)
        {

            if(!empty(Input::file()[$field]))
            {
                $file = Input::file()[$field];
                if(strstr($file->getMimeType(),'image') !== false)
                {
                    $ext = $file->getClientOriginalExtension();
                    $fileName = sha1($field.$id).'.'.$ext;
                    $filePath = $this->getUploadPath() . '/' . $id . '/' . $fileName;

                    $img = Image::make($file);

                    if(method_exists($this, 'onImageMaking')) {
                        $this->onImageMaking($field, $file, $img);
                    }

                    $img->save($filePath);

                    $uploadData = array_merge($uploadData, [
                        $field.'_url' => $filePath,
                        $field.'_filename' => $file->getClientOriginalName(),
                    ]);
                }
            }

            //delete
            if(!empty($request[$field.'_delete']))
            {
                $filePath = !empty($instance[$field.'_url']) ? $instance[$field.'_url'] : $filePath;
                unlink($filePath);
                $uploadData = array_merge($uploadData, [
                    $field.'_url' => '',
                    $field.'_filename' => '',
                ]);
            }

        }

        if(in_array('App\TraitLocalizableModel', class_uses(get_class($instance)))){
            $uploadData = array_merge($uploadData, [
                'locale' => $request->locale,
            ]);
        }


        $instance->update($uploadData);
    }

    public function getUploadPath() {
        return property_exists($this, 'uploadPath') ? $this->uploadPath : 'photo';
    }

    private function createUploadPath($id) {
        if(!is_dir($this->getUploadPath())){
            mkdir($this->getUploadPath());
        }

        if(!is_dir($this->getUploadPath().'/'.$id)){
            mkdir($this->getUploadPath().'/'.$id);
        }
    }

}
