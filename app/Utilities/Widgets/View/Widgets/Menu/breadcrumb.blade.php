@set('breadcrumb', isset($breadcrumb) ? $breadcrumb : [])

<ol class="breadcrumb">
    @foreach($breadcrumb['items'] as $key => $item)
        @if( $key == count($breadcrumb) )
            <li class="active">
                <span>{{ $item['label'] }}</span>
            </li>
        @else
            <li>
                <a href="{{ $item['realPath'] }}">
                    @if($key == 0 and !empty($breadcrumb['home_icon']))
                        <i class="fa fa-{{$breadcrumb['home_icon']}}"></i>
                    @endif
                    <span>{{ $item['label'] }}</span>
                </a>
            </li>
        @endif
    @endforeach
</ol>
