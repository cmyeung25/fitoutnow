<?php
namespace App\Utilities\Widgets\View\Widgets\Menu;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use App;
use Closure;

class MenuItem implements Arrayable {
    protected $url;
    protected $label;
    protected $class;
    protected $children;
    protected $isActive;
    protected $urlPrefix;
    protected $hide = false;

    public function __construct($label, $url = null, $class = null) {
        $this->setLabel($label);
        $this->setUrl($url);
        $this->setClass($class);
        $this->isActive = false;
        $this->children = new Collection();
    }

    public function getUrl() {
    	return $this->url;
    }

    public function setUrl($url) {
    	$this->url = $url;
        $this->isActive = $this->checkIsActive();
    	return $this;
    }

    public function getLabel() {
    	return $this->label;
    }

    public function setLabel($label) {
    	$this->label = $label;
    	return $this;
    }

    public function getClass() {
    	return $this->class;
    }

    public function setClass($class) {
    	$this->class = $class;
    	return $this;
    }

    public function getChildren($ignoreHide = false) {

        if($ignoreHide) {
            $children = new Collection;
            foreach ($this->children as $child) {
                if(!$child->isHide()) {
                    $children->push($child);
                }
            }
        } else {
            $children = $this->children;
        }
    	return $children;
    }

    public function addChild($label, $url = null, Closure $callback = null) {
        $menuItem = (new MenuItem($label, $url))->setUrlPrefix($this->urlPrefix);
        $this->children->push($menuItem);
        if( null != $callback ) {
            $callback($menuItem);
        }
    	return $this;
    }

    public function getIsChildrenActive() {
        $active = false;
        foreach ($this->children as $child) {
            if($child->isActive())
                $active = true;
        }
        return $active;
    }

    public function isActive() {
        return $this->isActive;
    }

    public function checkIsActive() {
        $request = App::make('request');
        // var_dump($request->getPathInfo(),$this->url, $this->urlPrefix);
        $url = [];
        if ( $this->urlPrefix !== null) {
            array_push($url, $this->urlPrefix);
        }

        if ( $this->url !== null ) {
            array_push($url, $this->url);
        }

        $url = '/'.implode('/', $url);

        $this->realPath = !empty($url)? $url : null ;

        $urlSegment = explode('/', $url);
        $requestPathSegment = explode('/',$request->getPathInfo());

        foreach ($urlSegment as $key => $value) {
            if($value == '{#}' and isset($requestPathSegment[$key])) {
                unset($requestPathSegment[$key]);
                unset($urlSegment[$key]);
            }
        }

        $url = implode('/', $urlSegment);
        $requestPath = implode('/', $requestPathSegment);

        return $url == $requestPath;
    }

    public function setUrlPrefix($urlPrefix) {
        $this->urlPrefix = $urlPrefix;
        $this->isActive = $this->checkIsActive();
        return $this;
    }

    public function hide() {
        $this->hide = true;
        return $this;
    }

    public function show() {
        $this->hide = false;
        return $this;
    }

    public function isHide() {
        return $this->hide;
    }

    public function toArray($ignoreHide = true) {
        return [
            'url' => $this->url,
            'label' => $this->label,
            'class' => $this->class,
            'children' => $this->getChildren($ignoreHide)->toArray($ignoreHide),
            'isActive' => $this->isActive or $this->getIsChildrenActive(),
            'urlPrefix' => $this->urlPrefix,
            'realPath' => !empty($this->url) ? url($this->realPath) : null,
        ];
    }
}
