<?php
namespace App\Utilities\Widgets\View\Widgets\Menu;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Closure;

class Menu implements Arrayable {
    protected $menuItems;
    protected $urlPrefix;
    protected $baseItem; //for display for breadcrumb
    protected $breadcrumbHomeIcon = 'dashboard';

    public function __construct($urlPrefix = null) {
        $this->menuItems = new Collection;
        $this->setUrlPrefix($urlPrefix);
    }

    public function addMenuItem($label, $url = null, Closure $callback = null )
    {
        $menuItem = (new MenuItem($label, $url))->setUrlPrefix($this->urlPrefix);
        $this->menuItems->push($menuItem);
        if( null != $callback ) {
            $callback($menuItem);
        }
        return $this;
    }


    public function getMenuItems() {
   		return $this->menuItems;
   	}

    public function setUrlPrefix($urlPrefix) {
		$this->urlPrefix = $urlPrefix;
		return $this;
	}

    public function toArray($ignoreHide = true) {
        $return = [];
        foreach ($this->menuItems as $item) {
            array_push($return, $item->toArray($ignoreHide));
        }
        return $return;
    }

    public function setBaseItem($label, $url = null) {
        $baseItem = (new MenuItem($label, $url))->setUrlPrefix($this->urlPrefix);
        $this->baseItem = $baseItem;
        return $this;
    }

    public function getBaseItem() {
        return $this->baseItem;
    }

    public function setBreadcrumbHomeIcon($icon = null) {
        $this->breadcrumbHomeIcon = $icon;
        return $this->breadcrumbHomeIcon;
    }

    public function getBreadcrumb() {
        $breadcrumbItems = [];
        $baseItem = $this->getBaseItem();
        if(!empty($baseItem))
        {
            array_push($breadcrumbItems, $baseItem->toArray());
        }

        $menuItems = $this->toArray(false);
        foreach ($menuItems as $item) {
            if($item['isActive']){
                array_push($breadcrumbItems,$item);
                $this->getBreadcrumbItems($item['children'],$breadcrumbItems);
            }
        }

        $breadcrumb = [
            'items' => $breadcrumbItems,
            'home_icon' => $this->breadcrumbHomeIcon,
        ];
        
        return $breadcrumb;
    }

    protected function getBreadcrumbItems($menuItems, &$breadcrumb) {
        foreach ($menuItems as $item) {
            if($item['isActive']){
                array_push($breadcrumb,$item);
                $this->getBreadcrumbItems($item['children'],$breadcrumb);
            }
        }
    }
}
