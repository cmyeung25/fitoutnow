<?php
namespace App\Utilities\Widgets\View\Widgets\Paginator;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class Paginator implements Arrayable {
    protected $data;

    public function __construct(LengthAwarePaginator $data) {
        $request = Request::createFromGlobals();
        $currentQuery = $request->query();
        $data->appends($currentQuery);
        $this->data = $data;
    }

    public function toArray() {
        return array_merge($this->data->toArray(), [
            'first_page_url' => $this->data->url(1),
            'last_page_url' => $this->data->url($this->data->lastPage()),
        ]);
    }
}
