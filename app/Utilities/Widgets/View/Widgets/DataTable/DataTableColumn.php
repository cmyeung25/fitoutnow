<?php

namespace App\Utilities\Widgets\View\Widgets\DataTable;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Stringy\Stringy;

class DataTableColumn implements Arrayable
{
    protected $field;
    protected $table;
    protected $name;
    protected $sortByUrl;
    protected $sortByDirection;
    protected $currentSort;
    protected $sortable = true;
    protected $displayFieldClosure;

    public function __construct($field, $name = null, $displayFieldClosure = null)
    {
        if (strstr($field, '.') != false) {
            $this->table = explode('.', $field)[0];
            $this->field = explode('.', $field)[1];
        } else {
            $this->field = $field;
        }

        if ($name == null) {
            $name = Stringy::create($this->field);
            $this->name = $name->humanize();
        } else {
            $this->name = $name;
        }

        // sorting
        $request = Request::createFromGlobals();
        $sortBy = $request->get('sort');
        $d = $request->get('d');

        $sortByDirection = 'ASC';
        if ($sortBy == $field) {
            $sortByDirection = ($d == 'ASC' ? 'DESC' : 'ASC');
            $this->currentSort = $d;
        }

        $urlQuery = array_merge($request->query(), [
            'sort' => $field,
            'd' => $sortByDirection,
        ]);
        $this->sortByDirection = $sortByDirection;
        $this->sortByUrl = $request->url().'?'.http_build_query($urlQuery);
        $this->setDisplayFieldClosure($displayFieldClosure);
    }

    public function getField()
    {
        return $this->field;
    }
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    public function getTable()
    {
        return $this->name;
    }
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    public function getSortable()
    {
        return $this->sortable;
    }

    public function setSortable($sortable)
    {
        $this->sortable = $sortable;

        return $this;
    }

    public function setDisplayFieldClosure($displayFieldClosure)
    {
        if ($displayFieldClosure == null) {
            $displayFieldClosure = function ($value) {
                return $value;
            };
        }
        $this->displayFieldClosure = $displayFieldClosure;

        return $this;
    }

    public function toArray()
    {
        return [
            'field' => $this->field,
            'name' => $this->name,
            'sortByUrl' => $this->sortByUrl,
            'sortByDirection' => strtolower($this->sortByDirection),
            'currentSort' => strtolower($this->currentSort),
            'sortable' => $this->sortable,
            'displayField' => $this->displayFieldClosure,
        ];
    }
}
