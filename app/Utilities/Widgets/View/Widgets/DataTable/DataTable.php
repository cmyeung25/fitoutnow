<?php

namespace App\Utilities\Widgets\View\Widgets\DataTable;

use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Arrayable;
use App\Utilities\Widgets\View\Widgets\DataTable\DataTableAction;

class DataTable implements Arrayable
{
    protected $data;
    protected $columns;
    protected $actions;
    protected $actionBaseUrl;
    protected $showActions = true;

    public function __construct($data)
    {
        if (!$data instanceof Collection) {
            $data = new Collection($data);
        }

        $this->data = $data;
        $this->columns = new Collection();
        $this->actions = new Collection();

        $this->actions->push(new DataTableAction('Edit', '<span class="btn btn-warning btn-xs">Edit</span>', '/detail/{#id}'));
        $this->actions->push(new DataTableAction('Delete', '<span class="btn btn-danger btn-xs">Delete</span>', '/delete/{#id}'));
    }

    public function toArray()
    {
        return [
            'columns' => $this->columns->toArray(),
            'data' => $this->data->toArray(),
            'actions' => $this->actions->toArray(),
            'isShow' => $this->showActions,
        ];
    }

    public function setColumns(array $columns)
    {
        foreach ($columns as $column) {
            if (!is_array($column)) {
                $column = new DataTableColumn($column);
            } else {
                $column['field'] = !isset($column['field']) ? null : $column['field'];
                $column['name'] = !isset($column['name']) ? null : $column['name'];
                $column['displayFieldClosure'] = !isset($column['displayFieldClosure']) ? null : $column['displayFieldClosure'];
                $column = new DataTableColumn($column['field'], $column['name'], $column['displayFieldClosure']);
            }

            $this->columns->push($column);
        }

        return $this;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function setActionBaseUrl($actionBaseUrl)
    {
        $this->actionBaseUrl = $actionBaseUrl;
        foreach ($this->actions as $action) {
            $action->setBaseUrl($actionBaseUrl);
        }

        return $this;
    }

    public function showActionColumn()
    {
        $this->showActions = true;

        return $this;
    }

    public function hideActionColumn()
    {
        $this->showActions = false;

        return $this;
    }

    public function setAction(DataTableAction $dataTableAction) {
        $this->actions->push($dataTableAction);
    }

    public function unsetAction($actionName) {
        $newActionCollection = new Collection();
        $this->actions->each(function($item,$key) use ($newActionCollection, $actionName){
            if($item->getActionName() != $actionName) {
              $newActionCollection->push($item);
            }
        });
        $this->actions = $newActionCollection;
    }

    protected function getActionBaseUrl()
    {
        return $this->actionBaseUrl;
    }
}
