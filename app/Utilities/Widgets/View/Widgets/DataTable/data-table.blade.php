@if(!empty($dataTable['data']))
<table class="data-table table table-hover">
    <thead>
        <tr>
            @foreach ($dataTable['columns'] as $column)
                <th class="{{ $column['sortable'] ? 'sortable' : '' }} {{ $column['currentSort'] }}" ><a href="{{ $column['sortByUrl'] }}">{{ $column['name'] }}</a></th>
            @endforeach
            <th>
                <span>Actions</span>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($dataTable['data'] as $row)
            <tr>
                @foreach ($dataTable['columns'] as $column)
                    <td>{!! $column['displayField']($row[$column['field']]) !!}</td>
                @endforeach
                <td>
                    @foreach ($dataTable['actions'] as $action)
                        <a {!! !empty($action['linkAttr']) ? $action['linkAttr']($row) : "" !!} href="{{ str_replace('{#id}', $row['id'] ,$action['url']) }}">{!! $action['displayContent'] !!}</a>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@else
    <p>Sorry. No Data available...</p>
@endif
