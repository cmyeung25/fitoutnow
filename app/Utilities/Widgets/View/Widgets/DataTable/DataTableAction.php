<?php
namespace App\Utilities\Widgets\View\Widgets\DataTable;

use Illuminate\Contracts\Support\Arrayable;

class DataTableAction implements Arrayable
{
    protected $actionName;
    protected $url;
    protected $baseUrl;
    protected $displayContent;
    protected $linkAttr;

    public function __construct($actionName, $displayContent, $url) {
        $this->displayContent = $displayContent;
        $this->actionName = $actionName;
        $this->url = $url;
    }

    public function toArray() {
        return [
            'actionName' => $this->actionName,
            'url' => $this->baseUrl. $this->url,
            'displayContent' => $this->displayContent,
            'linkAttr' => $this->linkAttr,
        ];
    }

    public function getUrl() {
   		return $this->url;
   	}
    public function setUrl($url) {
   		$this->url = $url;
   		return $this;
   	}
    public function getLinkAttr() {
      return $this->linkAttr;
    }
    public function setLinkAttr($linkAttr) {
      $this->linkAttr = $linkAttr;
      return $this;
    }

    public function getDisplayContent() {
   		return $this->displayContent;
   	}
    public function setDisplayContent($displayContent) {
   		$this->displayContent = $displayContent;
   		return $this;
   	}
    public function getActionName() {
   		return $this->actionName;
   	}
    public function setActionName($actionName) {
   		$this->actionName = $actionName;
   		return $this;
   	}

    public function getBaseUrl() {
   		return $this->baseUrl;
   	}
    public function setBaseUrl($baseUrl) {
   		$this->baseUrl = $baseUrl;
   		return $this;
   	}
}
