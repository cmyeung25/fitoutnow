{{--
$field_class
$field_placeholder
$field_value
$field_id
$field_label
$field_name
--}}

@set('field_class', isset($field_class) ? $field_class : '')
@set('field_input_class', isset($field_input_class) ? $field_input_class : 'form-control')
@set('field_placeholder', isset($field_placeholder) ? $field_placeholder : '')
@set('field_value', isset($field_value) ? $field_value : '')
@set('field_type', isset($field_type) ? $field_type : 'text')
@set('field_disabled', (isset($field_disabled) and $field_disabled) ? true : false)
@set('field_attr', isset($field_attr) ? $field_attr : [])
@set('field_name', isset($field_name) ? $field_name : '')
@set('field_error_message_attr', isset($field_error_message_attr) ? $field_error_message_attr : [])
<div class="form-group text {{ $field_class }}">
    <label for="{{ $field_id }}">{!! $field_label !!}</label>
    <input type="{{ $field_type }}" class="{{ $field_input_class }}" id="{{ $field_id }}" name="{{ $field_name }}" placeholder="{{ $field_placeholder }}" value="{{ $field_value }}" name="{{ $field_name }}" {{ $field_disabled ? 'disabled' : ''}}
        @foreach($field_attr as $key => $value)
            {{$key}}="{{$value}}"
        @endforeach
    >
    @yield('field_after_input')
    <span class="error-message"
        @foreach($field_error_message_attr as $key => $value)
            {{$key}}="{{$value}}"
        @endforeach
    ></span>
</div>
