{{--
$field_class
$field_placeholder
$field_value
$field_id
$field_label
$field_name
--}}

@set('field_class', isset($field_class) ? $field_class : '')
@set('field_value', isset($field_value) ? $field_value : '')
@set('field_disabled', (isset($field_disabled) and $field_disabled) ? true : false)
@set('field_attr', isset($field_attr) ? $field_attr : [])
@set('field_resizeable', isset($field_resizeable) ? $field_resizeable : false)
<div class="form-group text {{ $field_class }}">
    <label for="{{ $field_id }}">{{ $field_label }}</label>
    <textarea class="form-control {{ $field_resizeable ? 'resizeable' : ''}}" id="{{ $field_id }}" name="{{ $field_name }}" {{ $field_disabled ? 'disabled' : ''}}
        @foreach($field_attr as $key => $value)
            {{$key}}={{$value}}
        @endforeach
    >{{ $field_value }}</textarea>
    <span class="error-message"></span>
</div>
