{{--
$field_class
$field_value
$field_label
$field_name
--}}

@set('field_class', isset($field_class) ? $field_class : '')
@set('field_value', isset($field_value) ? $field_value : '')
@set('field_attr', isset($field_attr) ? $field_attr : [])
@set('field_label_attr', isset($field_label_attr) ? $field_label_attr : [])
@set('field_icon_backgound_color', isset($field_icon_backgound_color) ? $field_icon_backgound_color : 'blue')

<div class="form-group radio {{ $field_class }}">
    <label
        @foreach($field_label_attr as $key => $value)
            {{$key}}={{$value}}
        @endforeach
        >
        <input type="radio" value="{{ $field_value }}" name="{{ $field_name }}"
            @foreach($field_attr as $key => $value)
                {{$key}}={{$value}}
            @endforeach
        >
        <span class="{{ $field_icon_backgound_color }}">{!! $field_label !!}</span>
    </label>
    @if( !empty($field_hidden_value) )
        <input type="hidden" name="{{ $field_name }}" value="{{ $field_hidden_value }}">
    @endif
</div>
