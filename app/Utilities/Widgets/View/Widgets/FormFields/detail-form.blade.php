@set('form_class', isset($form_class) ? $form_class : '')


<form class="{{ $form_class }}" action="{{ $form_action }}" method="post">
    @yield('fields')
    <input type="hidden" name="id" value="{{ $data['id'] }}">
    @if (!empty($locale))
    <input type="hidden" name="locale" value="{{ $locale }}">
    @endif
    {{csrf_field()}}
    <div class="form-group">
        <span class="message"></span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Sumbit</button>
    </div>
</form>
