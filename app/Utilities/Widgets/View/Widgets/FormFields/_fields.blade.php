@embed ('Widgets.FormFields.text', [
    'field_placeholder' => '',
    'field_value' => $data['name'],
    'field_id' => '',
    'field_label' => 'Name',
    'field_name' => 'sync[name]',
])
@endembed
@embed ('Widgets.FormFields.select', [
    'field_value' => $data['continent'],
    'field_id' => 'continent',
    'field_label' => 'Continent',
    'field_name' => 'continent',
    'field_options' => $continents,
])
@endembed
@embed ('Widgets.FormFields.map-picker', [
    'field_id' => 'country',
    'field_label' => 'Map Location',
    'field_zoom' => '6',
    'field_lat_name' => 'latitude',
    'field_lat_value' => $data['latitude'],
    'field_lng_name' => 'longitude',
    'field_lng_value' => $data['longitude'],
])
@endembed
