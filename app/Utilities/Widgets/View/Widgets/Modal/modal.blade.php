@set('modal_has_close_button', isset($modal_has_close_button) ? $modal_has_close_button : true)
@set('modal_has_header', isset($modal_has_header) ? $modal_has_header : true)
@set('modal_has_footer', isset($modal_has_footer) ? $modal_has_footer : true)
@set('modal_test', isset($modal_test) ? $modal_test : false)
@set('modal_header_class', isset($modal_header_class) ? $modal_header_class : '')
@set('modal_close_button_color', isset($modal_close_button_color) ? $modal_close_button_color : '')
@set('modal_has_close_button', isset($modal_has_close_button) ? $modal_has_close_button : true)
@set('modal_size', isset($modal_size) ? $modal_size : '')

@if(!$modal_test)
<div class="modal fade" id="{{ $modal_id }}">
@endif
    <div class="modal-dialog {{ $modal_size }}">
        <div class="modal-content">
            @if($modal_has_header)
                <div class="modal-header  {{ $modal_header_class }}">
                    @yield('modal-header')
                    @if($modal_has_close_button)
                        <button type="button" class="close-button {{ $modal_close_button_color }}" data-dismiss="modal" aria-hidden="true">&times;</button>
                    @endif
                </div>
            @endif
            <div class="modal-body">
                 @yield('modal-body')
            </div>
            @if($modal_has_footer)
                <div class="modal-footer">
                    @yield('modal-footer')
                </div>
            @endif
        </div>
    </div>
@if(!$modal_test)
</div>
@endif
