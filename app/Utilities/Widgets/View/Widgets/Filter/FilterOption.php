<?php

namespace App\Utilities\Widgets\View\Widgets\Filter;

use Illuminate\Support\Collection;
// use Illuminate\Contracts\Support\Arrayable;
use ArrayAccess;

class FilterOption implements ArrayAccess
{
    public function __construct($data)
    {
      $this->data = $data;
      $this->container = [];
      $this->container['name'] = $data['name'];
      $this->container['key'] = $data['key'];
      $this->container['count'] = $data['count'];
      $this->container['url'] = '';
      $this->container['visible'] = true;
    }


    public function offsetSet($offset, $value) {
      if (is_null($offset)) {
         $this->container[] = $value;
      } else {
         $this->container[$offset] = $value;
      }
    }

    public function offsetExists($offset) {
       return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
       unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
       return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

}
