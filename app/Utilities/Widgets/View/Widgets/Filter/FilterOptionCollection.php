<?php

namespace App\Utilities\Widgets\View\Widgets\Filter;

use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Arrayable;
use App\Utilities\Widgets\View\Widgets\Filter\FilterOption;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Closure;

class FilterOptionCollection implements Arrayable
{
    protected $collection;
    protected $query;
    protected $key;
    protected $name;
    protected $urlRule;
    protected $currentFilter;
    protected $collectionVisableCount;

    public function __construct()
    {
      $this->collection = new Collection();
      $this->urlRule = function($key, $item) {
        return "{$key}={$item['key']}";
      };
      $this->currentFilter = [];
      $this->collectionVisableCount = 0;
    }

    public function setCountFilter(Builder $query, $callback = null) {
      if(!empty($callback)) {
        $this->query = $callback($query);
      } else {
        $this->query = $query;
      }
      return $this;
    }

    public function push(FilterOption $option) {
      $this->collection->push($option);
    }

    public function calc($callback = null) {
      if(!empty($callback)) {
        $this->query = $callback($this->query, $this);
      }
      $groupedResult = $this->query->get();
      $this->collectionVisableCount = 0;
      foreach ($groupedResult as $result) {
        $option = $this->collection->where('key',$result->key)->first();
        if(!empty($option)) {
          $option['count'] = $result->count;
          $option['url'] = $this->buildUrl($option);

          if($option['count'] > 0) {
            $this->collectionVisableCount++;
          }
        }
      }
    }

    protected function buildUrl(&$option) {
      $request = Request::createFromGlobals();
      $getData = $request->all();
      $newFilterPattern = explode('=', ($this->urlRule)($this->key, $option));
      if(!empty($getData[$newFilterPattern[0]])) {
        if($getData[$newFilterPattern[0]]!=$newFilterPattern[1]) {
          $getData[$newFilterPattern[0]] = $getData[$newFilterPattern[0]] . '|' . $newFilterPattern[1];
        } else {
          $option['visible'] = false;
          $getData2 = $request->all();

          unset($getData2[$newFilterPattern[0]]);
          $this->currentFilter[] = [
              'key' => $this->key,
              'option' => $option,
              'removeUrl' => '?'. http_build_query($getData2),
          ];
          $this->collectionVisableCount--;
        }
      } else {
        $getData[$newFilterPattern[0]] = $newFilterPattern[1];
      }
      return '?'. http_build_query($getData);
    }

    public function getResult() {
      return $this->collection;
    }

    public function setKey($key) {
      $this->key = $key;
      return $this;
    }

    public function setName($name) {
      $this->name = $name;
      return $this;
    }

    public function setUrlRule(Closure $rule) {
      $this->urlRule = $rule;
      return $this;
    }

    public function isVisible() {
      return $this->collectionVisableCount > 0;
    }

    public function getCurrentFilter() {
      return $this->currentFilter;
    }

    public function toArray()
    {
        return [

        ];
    }
}
