<div class="box @yield('box-class')">
    <div class="box-header">
        <div class="box-title">
            @yield('box-title')
        </div>
        <div class="box-tools">
            @yield('box-tools')
        </div>
    </div>
    <div class="box-body">
        @yield('box-body')
    </div>
</div>
