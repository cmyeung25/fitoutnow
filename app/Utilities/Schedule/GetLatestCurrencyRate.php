<?php

namespace App\Utilities\Schedule;
use Storage;

class GetLatestCurrencyRate
{
  public function __invoke() {
    $currencies = ["HKD","CAD","GBP","JPY","THB","EUR","MYR","CNY","USD","SGD","AUD","KRW"];

    $rates = [
      'last_update'=> date("Y-m-d"),
      'rates' => []
    ];
    foreach ($currencies as $key => $currency) {
      $result = file_get_contents("https://api.exchangeratesapi.io/latest?base={$currency}");
      $json = json_decode($result,true);
      $rates['rates'][$currency] = $json["rates"];
    }

    Storage::disk('local')->put('rate.json', json_encode($rates));
  }
}
