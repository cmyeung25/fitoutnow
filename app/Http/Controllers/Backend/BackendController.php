<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
// use Carbon\Carbon;

class BackendController extends BackendBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('backend/dashboard');
    }

    public function dashboard()
    {
      return view('backend.dashboard');
    }
}
