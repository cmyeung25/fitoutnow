<?php

namespace App\Http\Controllers\Backend;

use Session;
use Validator;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Config;
use Auth;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Utilities\Traits\TraitRestFromValidationController;


class AuthController extends BackendBaseController
{
  use TraitRestFromValidationController;

  public function getIndex(Request $request) {
    return redirect('backend/auth/login');
  }
  public function postLogin(Request $request) {
    $validator = Validator::make($request->all(), []);

    if($validator->fails()) {
        return static::failValidation($validator->getMessageBag());
    }
    // If the class is using the ThrottlesLogins trait, we can automatically throttle
    // the login attempts for this application. We'll key this by the username and
    // the IP address of the client making these requests into this application.
    $throttles = $this->isUsingThrottlesLoginsTrait();

    if ($throttles && $this->hasTooManyLoginAttempts($request)) {
        return $this->sendLockoutResponse($request);
    }

    $credentials = $this->getCredentials($request);

    if (Auth::attempt($credentials, $request->has('remember')) &&
            $this->loginExtraValidation(Auth::user())
        ) {
        return $this->handleUserWasAuthenticated($request, $throttles);
    }

    if ($throttles) {
        $this->incrementLoginAttempts($request);
    }

    return static::failWithMessage($this->getFailedLoginMessage());
  }

  protected function getCredentials(Request $request)
  {
      return $request->only('email', 'password');
  }

  public function getLogin(Request $request) {
    $data = [];
    $data['actionUrl'] = "";
    return view("backend/auth/login",$data);
  }
  public function getLogout(Request $request) {
    Auth::logout();
    return redirect("backend/auth/login");
  }


    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    protected function loginExtraValidation(AuthenticatableContract $user) {
        return true;
    }

    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
      if ($throttles) {
          $this->clearLoginAttempts($request);
      }

      if (method_exists($this, 'authenticated')) {
          $this->authenticated($request, Auth::user());
      }

      $redirectUrl = url("backend");

      if($request->session()->has('login-redirect')){
          $redirectUrl = url($request->session()->pull('login-redirect'));
      }
      return static::successWithRedirect($redirectUrl, 'Login Successful');
  }

  protected function getFailedLoginMessage()
  {
      return Lang::has('auth.failed')
              ? Lang::get('auth.failed')
              : 'These credentials do not match our records.';
  }
}
