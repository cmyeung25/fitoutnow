<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Utilities\Traits\TraitBaseCrudController;

use App\Utilities\Widgets\View\Widgets\DataTable\DataTable;
use App\Utilities\Widgets\View\Widgets\DataTable\DataTableAction;
use App\Models\User;
use App\Models\Company;
use Validator;
use Auth;

class UserController extends BackendBaseController
{
    use TraitBaseCrudController {
        TraitBaseCrudController::getListing as traitGetListing;
    }

    protected $model = User::class;
    protected $viewPath = 'backend.user';
    protected $baseRoute = '/user';
    protected $modelDataName = 'data';
    protected $backendController = true;

    public function getListing(Request $request)
    {
        return $this->traitGetListing($request, function ($query, $locale) {
            // $query->with('trips');
            // $query->with(['city' => function ($query) use ($locale) {
            //     $query->joinI18nWithLocale($locale);
            // }]);
        });
    }

    protected function getDataTableColumns()
    {
        return [
            ['field' => 'users.id'],
            ['field' => 'users.name'],
            ['field' => 'users.email'],
            ['field' => 'users.phone'],
            ['field' => 'users.first_name'],
            ['field' => 'users.last_name'],
            // ['field' => 'users.login_type', 'displayFieldClosure' => function ($value) {
            //     $icon = User::getLoginTypeIcon($value);
            //     if (null !== $icon) {
            //         $return = "<i title=\"{$value}\" class=\"fa fa-{$icon}\"></i>";
            //     } else {
            //         $return = $value;
            //     }
            //
            //     return $return;
            // }],
            ['field' => 'users.status'],
            ['field' => 'users.created_at'],
            // ['field' => 'users.trips','name' => 'Related Trips','displayFieldClosure' => function ($value) {
            //     return count($value);
            // }],
        ];
    }


    protected function getKeywordSearchBuilder($query, $keyword)
    {
        $query->whereIn('users.id', function ($query) use ($keyword) {
            $query
            ->select('users.id')
            ->from('users')
            ->where('email', 'like', "%{$keyword}%")
            ;
        });

        return $query;
    }
    protected function configListingDataTable(DataTable &$dataTable) {
        $dataTable->setAction(new DataTableAction('View', '<span class="btn btn-primary btn-xs">View</span>', 'view/{#id}'));
        $dataTable->unsetAction('Delete');
    }

    //---- custom function
    public function getView(Request $request, $id) {
      $data = [];

      $data['data'] = User::query()
                      // ->with(['bills', 'payments.paymentMethod'])
                      ->with('company','companyIds')
                      ->where('id',$id)
                      ->first();

      $data['mappable_companies'] = Company::query()
                          ->whereNotIn('id',$data['data']['companyIds']->pluck('company_id')->toArray())
                          ->get();

      return view("{$this->viewPath}.view",$data);
    }

    public function getRemoveMappingCompany(Request $request, $userId, $companyId) {
      $user = User::find($userId);
      $company = Company::find($companyId);
      $user->company()->detach($company);

      return redirect($this->getBaseRoute().'/view/'.$userId);
    }

    public function postInsertMappingCompany(Request $request) {
      $postData = $request->all();
      if(!empty($postData['user_id']) && !empty($postData['company_id'])) {
        $user = User::find($postData['user_id']);
        $company = Company::find($postData['company_id']);
        $user->company()->attach($company);
      }


      return redirect($this->getBaseRoute().'/view/'.$postData['user_id']);
    }

    public function postChangePassword(Request $request){
      $postData = $request->all();

      $errors = [];
      $validator = Validator::make($request->all(), [
          'user_id' => 'required',
          'password' => 'required|confirmed|min:6',
      ]);

      if ($validator->fails()) {
          $errors = $validator->getMessageBag();
      }

      if (!empty($errors)) {
          return redirect($this->getBaseRoute().'/detail/'.$postData['user_id'].'?error=1');
      }

      //update password
      $user = User::find($postData['user_id']);
      $user->password = bcrypt($postData['password']);
      $user->save();
      return redirect($this->getBaseRoute().'/detail/'.$postData['user_id'].'');
    }
}
