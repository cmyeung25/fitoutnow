<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utilities\Widgets\View\Widgets\Menu\Menu;
use Config;
use View;

abstract class BackendBaseController extends Controller
{
    public function __construct(Request $request)
    {
        $menu = new Menu(Config::get('backend.url'));
        $menu->setBaseItem('Dashboard', '/');
        $menu
        ->addMenuItem('User', null, function ($menuItem) {
            $menuItem->setClass('header');
        })
        ->addMenuItem('User', 'user/listing', function ($menuItem) {
            $menuItem
                ->addChild('User Detail', 'user/detail/{#}', function ($menuItem) {
                    $menuItem->hide();
                });
        })
        ->addMenuItem('Company', 'company/listing', function ($menuItem) {
            $menuItem
                ->addChild('Company Detail', 'company/detail/{#}', function ($menuItem) {
                    $menuItem->hide();
                });
        })
        ->addMenuItem('Asset', 'asset/listing', function ($menuItem) {
            $menuItem
                ->addChild('Asset Detail', 'asset/detail/{#}', function ($menuItem) {
                    $menuItem->hide();
                });
        })
        ->addMenuItem('Site Content', null, function ($menuItem) {
            $menuItem->setClass('header');
        })
        ->addMenuItem('Mall', 'mall/listing', function ($menuItem) {
            $menuItem
          ->addChild('Mall Detail', 'mall/detail/{#}', function ($menuItem) {
              $menuItem->hide();
          });
        })
        ->addMenuItem('Admin', null, function ($menuItem) {
            $menuItem->setClass('header');
        })
        ->addMenuItem('Feedback', 'feedback/listing', function ($menuItem) {
            $menuItem
          ->addChild('Feedback Detail', 'feedback/detail/{#}', function ($menuItem) {
              $menuItem->hide();
          });
        })
        ;


        View::share('side_menu', $menu->toArray());
        View::share('breadcrumb', $menu->getBreadcrumb());
        View::share('logout_url', url('/backend/auth/logout'));
    }

    protected function backendUrl($uri)
    {
        return url(Config::get('backend.url').'/'.$uri);
    }
}
