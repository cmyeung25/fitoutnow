<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use PHPHtmlParser\Dom;
use Illuminate\Support\Facades\View;
use App\Utilities\CommonFunction;
// use Dompdf\Dompdf;
use PDF;
use App\Models\Quotation;
use App\Notifications\QuotationCreated;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $request = (new Request)::createFromGlobals();
      View::share('get',$request->all());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
      $data = [];
      return view('home',$data);
    }

    public function getPdf(Request $request) {
      $data = [];
      $token = $request->get('token');

      $data['quotation'] = Quotation::where('quotation_token',$token)->get()[0];
      // $data['quotation']->versions;
      $font = PDF::getDomPDF()->getFontMetrics()->get_font("genshin", "bold");
      $pdf = PDF::loadView('pdf.quotation', ['data'=> $data]);
      return  $pdf->stream();
    }

    public function getEmail() {
      $quotation = Quotation::find(5);
      // dd($quotation);
      $quotation->notify(new QuotationCreated($quotation));

    }

}
