<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use Illuminate\Support\Collection;
use NumberFormatter;
use App\Models\Quotation;
use App\Utilities\Traits\TraitRestFromValidationController;
use App\Notifications\QuotationCreated;


class ApiController extends Controller
{
    use TraitRestFromValidationController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function postSaveQuotation(Request $request) {
      $data = [];
      $quotationData = $request->get("quotation");
      $regroupAnswserData = $request->get("regroupAnswser");

      $data['title'] = $quotationData['contactInfo']['title'];
      $data['name'] = $quotationData['contactInfo']['name'];
      $data['phone'] = $quotationData['contactInfo']['phone'];
      $data['email'] = $quotationData['contactInfo']['email'];

      $data['content'] = [
        "quotation" => $quotationData,
        "regroupAnswser" => $regroupAnswserData,
      ];
      // $data[""]

      if(!empty($quotationData['token'])) {
        $quotations = Quotation::where('quotation_token',$quotationData['token'])->get();
        $quotation = $quotations[0];
        $quotation->fill($data);
        $quotation->save();
      } else {
        $quotation = Quotation::create($data);
      }

      $quotation->notify(new QuotationCreated($quotation));

      $returnData = [
        "quotation_ref" => $quotation['quotation_ref'],
        "quotation_downloadURL" => "/pdf?token={$quotation['quotation_token']}",
      ];

      $returnMessage = "Quotation Created";

      return static::success($returnData,$returnMessage);
    }

    public function postInquiryQuotation(Request $request) {
      $ref = $request->get("ref");
      $phone = $request->get("phone");

      $quotations = Quotation::where('quotation_ref',$ref)->where('phone',$phone)->get();

      if($quotations->count() == 0) {
        return static::failWithMessage("沒有相關資料");
      } else {
        $quotation = $quotations[0];
        $returnData = [
          "quotation_ref" => $quotation['quotation_ref'],
          "quotation_downloadURL" => "/pdf?token={$quotation['quotation_token']}",
          "quotation_modifyURL" => "/modify?token={$quotation['quotation_token']}",
        ];

        $returnMessage = "找到相關資料";

        return static::success($returnData,$returnMessage);
      }
    }

    public function getQuotationByToken(Request $request) {
      $token = $request->get("token");

      $quotations = Quotation::where('quotation_token',$token)->get();

      if($quotations->count() == 0) {
        return static::failWithMessage("沒有相關資料");
      } else {
        $quotation = $quotations[0];
        $returnData = [
          'quotation' => $quotation,
        ];

        $returnMessage = "找到相關資料";

        return static::success($returnData,$returnMessage);
      }


    }

}
