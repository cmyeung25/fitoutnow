<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;
use App;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Blade::directive('date', function ($expression) {
        return "<?php echo ($expression)->format('Y-m-d'); ?>";
      });
      Blade::directive('price', function ($expression) {
        $value = number_format(floatval($expression));
        if(floatval($value) === 0) {
          return "<?php echo '-'; ?>";
        } else {
          return "
            <?php echo 'HKD'; ?>
            <?php echo number_format(floatval($expression)); ?>
          ";
        }
      });
    }
}
