<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Mpociot\Versionable\VersionableTrait;

class Quotation extends Model
{
    // use SoftDeletes;
    use Notifiable;
    use VersionableTrait;

    protected $table = 'quotations';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'content' => 'array',
    ];

    protected $fillable = [
      'quotation_ref',
      'quotation_token',
      'title',
      'name',
      'phone',
      'email',
      'content',
    ];

    public function routeNotificationForMail($notification) {
      return $this->email;
    }

    public static function create(array $data = []){
      $data['quotation_ref'] = static::getNewAssetId();
      $data['quotation_token'] = static::getNewToken($data);
      return parent::query()->create($data);
    }

    public static function getNewAssetId() {
      $count = static::count();
      $date = Date('Ymd');
      return $date . str_pad($count,5,'0',STR_PAD_LEFT);
    }

    public static function getNewToken($data) {
      $text = $data['quotation_ref'] . "_" . $data['phone'];
      return hash('sha256',$text);
    }
}
