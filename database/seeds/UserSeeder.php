<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // $user = User::create([
        //     'name' => 'admin',
        //     'email' => 'admin@admin.com',
        //     'password' => bcrypt('password'),
        //     'login_type' => 'internal',
        //     'type' => 'admin',
        // ]);
        // $company = new Company();
        // $company->save();
        // $user->company()->attach($company);

        $user = User::create([
          'first_name' => 'Admin',
          'last_name' => 'Admin',
          'name' => 'admin',
          'email' => 'admin@admin.com',
          'phone' => '22334455',
          'password' => bcrypt('password'),
          'type' => 'admin',
        ]);
    }

    protected function getNewUserRetrieveToken() {
      $token = sha1(date('ymdhis').'userRetrieve');
      if(!$this->checkTokenUnique($token)) {
        $token = $this->getNewUserRetrieveToken();
      }
      return $token;
    }

    protected function checkTokenUnique($token) {
      return User::where('account_retrieve_token',$token)->count() == 0;
    }
}
