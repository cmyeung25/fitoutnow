<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddUserRetrieveAccountDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function(Blueprint $table){
        $table->string('account_retrieve_token')->nullable();
        $table->timestamp('account_retrieved_at')->nullable();
        $table->boolean('email_validated')->default(false);
        $table->string('status')->default('active');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('users', function(Blueprint $table){
        $table->dropColumn('account_retrieve_token');
        $table->dropColumn('account_retrieved_at');
        $table->dropColumn('email_validated');
        $table->dropColumn('status');
      });
      if(Storage::disk('local')->exists('excel_import_cache.txt')) {
        Storage::disk('local')->delete('excel_import_cache.txt');
      }
    }
}
