<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->nullable();
          $table->string('email');
          $table->string('password', 60)->nullable();
          $table->string('login_type', 15)->default("internal");
          $table->string('activation_code')->nullable();
          $table->timestamp('activated_at')->nullable();
          $table->string('identifier')->nullable();
          $table->string('gender', 15)->nullable();
          $table->string('avatar')->nullable();
          $table->enum('type', ['admin', 'user'])->default('user');
          $table->rememberToken();
          $table->timestamps();
          $table->integer('user_group_id')->unsigned()->nullable(); //Required to remember to init once the user is create.

          $table->index('user_group_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
