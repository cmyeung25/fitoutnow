<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;


class AddDetailsOfUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
          $table->string('first_name')->nullable();
          $table->string('last_name')->nullable();
          $table->string('phone',15)->nullable();
          $table->boolean('phone_validated')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('users', function(Blueprint $table){
        $table->dropColumn('first_name');
        $table->dropColumn('last_name');
        $table->dropColumn('phone');
        $table->dropColumn('phone_validated');
      });
    }
}
