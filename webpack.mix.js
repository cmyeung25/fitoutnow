const mix = require('laravel-mix');
var bourbon = require('bourbon').includePaths;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.scripts([
   'resources/js/common/*.js',
 ],'public/js/common/common.js');

 mix.sass('resources/sass/backend.scss', 'public/css/backend.css', {
     includePaths: bourbon,
 });

 mix.sass('resources/sass/pdf.scss', 'public/css/pdf.css', {
    includePaths: bourbon,
});

 mix.scripts([
     'resources/vendor/plugins/jQuery/jQuery-2.1.4.min.js',
     'resources/vendor/plugins/jQueryUI/jquery-ui.min.js',
     'resources/vendor/bootstrap/js/bootstrap.min.js',
     'resources/vendor/plugins/daterangepicker/moment.min.js',
     'resources/vendor/plugins/daterangepicker/daterangepicker.js',
     'resources/vendor/plugins/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
     'resources/vendor/plugins/swiper/swiper.min.js',
     'resources/vendor/plugins/select2/select2.full.min.js',
     'resources/vendor/plugins/form/jquery.form.min.js',
     'resources/vendor/plugins/pickadate/picker.js',
     'resources/vendor/plugins/pickadate/picker.date.js',
     'resources/vendor/plugins/pickadate/picker.time.js',
     'resources/vendor/adminlte/js/app.min.js',
     'resources/vendor/plugins/summernote/summernote.min.js',
 ],'public/js/backend/vendor.js');

mix.react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
 mix.copyDirectory('resources/images','public/images');
mix.copyDirectory('resources/json','public/json');
mix.browserSync('http://www.fitoutnow.com');
