<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/backend', 'namespace' => 'Backend'], function () {
    Route::get('auth/','AuthController@getIndex');
    Route::post('auth/login','AuthController@postLogin');
    Route::get('auth/login','AuthController@getLogin');
    Route::get('auth/logout','AuthController@getLogout');
});

Route::group(['prefix' => '/backend', 'middleware' => 'backendAuth', 'namespace' => 'Backend'], function () {
  Route::get('/','BackendController@index');
  Route::get('/dashboard','BackendController@dashboard');

  Route::group(['prefix' => 'user',],function(){
    Route::get('index','UserController@getIndex');
    Route::get('listing','UserController@getListing');
    Route::get('detail/{id}','UserController@getDetail');
    Route::post('detail/{id}','UserController@postDetail');
    Route::get('delete/{id}','UserController@getDelete');

    //custom
    Route::get('view/{id}','UserController@getView');
    Route::get('remove-mapping-company/{userId}/{companyId}','UserController@getRemoveMappingCompany');
    Route::post('insert-mapping-company','UserController@postInsertMappingCompany');
    Route::post('change-password','UserController@postChangePassword');
  });
});



Route::get('/pdf','HomeController@getPdf');
Route::get('/email','HomeController@getEmail');

Route::get('/{path?}', [
    'uses' => 'HomeController@index',
    'as' => 'react',
    'where' => ['path' => '.*']
]);

// Route::get('/','HomeController@index');
// Route::get('/dashboard','HomeController@index');
// Route::get('/search-results','HomeController@index');

Auth::routes();
