@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Search Panel</div>

                <div class="card-body">
                    <form action="{{url('/search-results')}}" method="get">
                      <div class="form-group">
                        <div class="input-group mb-3">
                          <input type="text" name="keyword" class="form-control" placeholder="Input keywords">
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="sumbit"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
