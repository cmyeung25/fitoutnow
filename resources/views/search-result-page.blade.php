@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Search Result</div>
                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        @foreach ($siteResults as $site => $results)
                          <th>{{$site}}</th>
                        @endforeach
                      </tr>
                    </thead>
                    <tbody>
                      @for ($i = 0; $i < 10; $i++)
                        <tr>
                          <td rowspan="5">{{$i}}</td>
                          <td>Thumb</td>
                          @foreach ($siteResults as $site => $results)
                            <td>{!! !empty($results[$i]) ? "<img src=" . $results[$i]['thumb'] . "></a>" : "" !!}</td>
                          @endforeach
                        </tr>
                        <tr>
                          <td>Title</td>
                          @foreach ($siteResults as $site => $results)
                            <td>{{!empty($results[$i]) ? $results[$i]['title'] : ""}}</td>
                          @endforeach
                        </tr>
                        <tr>
                          <td>Price0</td>
                          @foreach ($siteResults as $site => $results)
                            <td>{{!empty($results[$i]) ? $results[$i]['price0'] : ""}}</td>
                          @endforeach
                        </tr>
                        <tr>
                          <td>Price1</td>
                          @foreach ($siteResults as $site => $results)
                            <td>{{!empty($results[$i]) ? $results[$i]['price1'] : ""}}</td>
                          @endforeach
                        </tr>
                        <tr>
                          <td>Price2</td>
                          @foreach ($siteResults as $site => $results)
                            <td>{{!empty($results[$i]) ? $results[$i]['price2'] : ""}}</td>
                          @endforeach
                        </tr>
                      @endfor
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
