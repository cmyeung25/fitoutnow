<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
        <title>Fitout Now 報價單：{{$data['quotation']['quotation_ref']}} </title>
        <link rel="stylesheet" href="{{ asset('css/pdf.css')}}">
    </head>
    <body>
        <h1 class="text-center">報價單</h1>
        @set('content', $data['quotation']['content'])
        @set('quotation', $content['quotation'])
        @set('regroupAnswser', $content['regroupAnswser'])
        <table class="quotation-header">
          <tr>
            <td>客人:</td>
            <td>{{$quotation['contactInfo']['name']}} {{$quotation['contactInfo']['title']}}</td>
            <td>報價單編號:</td>
            <td>{{$data['quotation']['quotation_ref']}}</td>
          </tr>
          <tr>
            <td>電話:</td>
            <td>{{$quotation['contactInfo']['phone']}}</td>
            <td>修改版本:</td>
            <td>{{$data['quotation']->versions->count()}}</td>
          </tr>
          <tr>
            <td>電郵:</td>
            <td>{{$quotation['contactInfo']['email']}}</td>
            <td>工程:</td>
            <td>{{$quotation['flat']['description']}}</td>
          </tr>
          <tr>
            <td>日期:</td>
            <td>@date($data['quotation']['created_at'])</td>
            <td></td>
            <td></td>
          </tr>
        </table>
        <table>
          <thead>
            <tr>
              <th>項目</th>
              <th>內容</th>
              <th>數量</th>
              <th>單位</th>
              <th>單價</th>
              <th>加總</th>
            </tr>
          </thead>
          @set('categoryIndex',1)
          @foreach ($regroupAnswser as $ckey => $categoryData)
            @set('answerIndex',1)
            @set('nonDisplayedCost',0)
            <tr>
              <td colspan="6">
                {{$categoryData['category']['title']}} {{$categoryData['category']['key']== "bedroom" ? $categoryData['roomIndex'] + 1 : ""}}
              </td>
              @foreach ($categoryData['groupedAnswers'] as $akey => $answerData)
                @if($answerData['isMultple']== true)
                  @set('answers',[])
                  @set('display',"")
                  @set('unit',"")
                  @set('inputUnit',"")
                  @set('unitPrice',"")
                  @set('cost',"")

                  @php
                    foreach ($answerData['answers'] as $i => $ivalue) {
                      $existingIndex = -1;
                      foreach ($answers as $j => $jvalue) {
                        if($ivalue['display'] == $jvalue['display']) {
                          $existingIndex= $j;
                        }
                      }

                      if($existingIndex > -1)  {
                        $answers[$existingIndex]['unit'] = $answers[$existingIndex]['unit'] + $ivalue['unit'];
                        $answers[$existingIndex]['cost'] = $answers[$existingIndex]['cost'] + $ivalue['cost'];
                      } else {
                        $answers[] = $ivalue;
                      }
                    }
                  @endphp
                  @foreach ($answers as $answer)
                    @if(!empty($answer['display']))
                      @set('display',$display.$answer['display']."<br />")
                      @set('unit',$unit.$answer['unit']."<br />")
                      @set('inputUnit',$inputUnit.$answer['inputUnit']."<br />")
                      @set('unitPrice',$unitPrice."HKD ".number_format($answer['unitPrice'])."<br />")
                      @set('cost',$cost."HKD ".number_format($answer['cost'])."<br />")
                    @else
                      @set('nonDisplayedCost',$nonDisplayedCost+$answer['cost'])
                    @endif
                  @endforeach
                  @if(!empty($display))
                    <tr>
                      <td>{{$categoryIndex}}.{{$answerIndex}}</td>
                      <td>{!! $display !!}</td>
                      <td>{!! $unit !!}</td>
                      <td>{!! $inputUnit !!}</td>
                      <td style="text-align:right">{!! $unitPrice !!}</td>
                      <td style="text-align:right">{!! $cost !!}</td>
                    </tr>
                  @endif
                  @set('answerIndex',$answerIndex+1)
                @else
                  @set('answer',$answerData['answer'])
                  @set('display',$answer['display'] ?? "")

                  @if(!empty($answer['selectedOption']))
                    @set('display',$display . "<br /> - " . $answer['selectedOption']['detail']['chineseDesc'])
                  @endif
                  @if(!empty($display))
                    <tr>
                      <td>{{$categoryIndex}}.{{$answerIndex}}</td>
                      <td>{!! $display !!}</td>
                      <td>{{$answer['unit']??""}}</td>
                      <td>{{$answer['inputUnit']??""}}</td>
                      <td style="text-align:right">@price($answer['unitPrice'])</td>
                      <td style="text-align:right">@price($answer['cost'])</td>
                    </tr>
                    @set('answerIndex',$answerIndex+1)
                  @endif
                @endif

              @endforeach
            </tr>
            @set('categoryIndex',$categoryIndex+1)
          @endforeach
          <tfoot>
            <tr>
              <td style="text-align:right" colspan="5">共數</td>
              <td style="text-align:right">@price($quotation['total'])</td>
            </tr>
          </tfoot>
        </table>
        <div class="break"></div>
        <h3><u>條款和條件</u></h3>
        <span>客人確認及簽訂報價單後, 以下條款或條件即時成立：</span>
        <ol>
          <li>
            未被提及的項目會被視為被屬於合約以外、包括:
            <ol>
                <li>供應牆身地面磚瓦 / 石材</li>
                <li>供應木地板</li>
                <li>供應大門鎖</li>
                <li>供應氣鼓</li>
                <li>供應門頂</li>
                <li>電燈開關或插座掣面</li>
                <li>電器</li>
                <li>與樓宇自動化控制系統連接的工程</li>
                <li>現有傢私之臨時安置/倉租費用</li>
            </ol>
          </li>
          <li>報價以未經更改之單位間隔原有圖則作準。</li>
          <li>除非本合同另有規定，否則大廈機電服務工程由樓宇指定分包商（NSCs）負責。</li>
          <li>如對項目的服務標準有有任何爭議，建議客戶向獨立的建築測量師/公證行尋求有關有爭議項目的第三方建議。</li>
          <li>當符合以下任何一項或多項、包括但不限於以下條件時，工程項目實屬已完工：
            <ol>
                <li>工藝質素已根據香港特別行政區建築工程合約的一般條件進行；</li>
                <li>客戶或其代表滿意地接受了項目；</li>
                <li>項目已移交給客戶代表或已被客戶方關連人士使用中；</li>
                <li>客戶的直接僱員，其子母公司或子公司的個人，和/或其他分包商和服務提供商佔用或使用中。</li>
            </ol>
          </li>
          <li>開工日期，物料安排及人手分配以客戶確認報價單後以及雙方確認交場予團隊施工為準。</li>
          <li>客戶須確保有適當工作空間及先決條件給予工程進行, 包括但不限於以下情況：
            <ol>
              <li>足够貨物運送通道及儲存空間；</li>
              <li>可到達樓層的貨物升降機/ 載客升降機；</li>
              <li>如沒有升降機/ 電梯到達樓層、須確保有樓梯及足够運送空間可到達、亦會收取額外附加費；</li>
              <li>如工程期間受任何其他本公司沒有能力控制的第三方工種阻礙而引致工程耽誤，Fitout Now將會額外收取客戶工程耽誤之費用</li>
            </ol>
          </li>
          <li>已移交給客戶的完成項目需由客戶或客方代表自行負責保護以免發生受外在影響而損壞。</li>
          <li>在完全清算付款之前，工程，商品和服務為 Fitout Now所擁有。</li>
          <li>簽署的合約或報價形成了Fitout Now和客戶的合約關係。違約金為確認及簽訂之報價單總價10%。</li>
          <li>正常工作時間：週一至週六上午9:00至下午6:00。 特殊工作時間可以協商，可能會產生額外費用。</li>
          <li>此報價單有效期為14天。</li>
          <li>客戶優惠：收取第二期付款後將減免50%設計項目費用。</li>
          <li>任何折扣所引致的價格改變除了適用於實收的金額上，也適用於之後其他優惠的減免金額上（如有）。</li>
          <li>工程預計時間: 設計圖紙及物料選項確認後60工作天。</li>
          <li>如受天災、戰爭、社會運動、工潮或疫症等不可抗力之影響，工程時間將受影響。服務、貨物到達及現場施功的時間將作另行通知。</li>
        </ol>
        <h3><u>執修期限:</u></h3>
        <span>此工程執修期為工程完工後30日</span>
        <h3><u>付款條件:</u></h3>
        <span>
          第一期：確定報價後收取合約款項的50%訂金。<br>
          第二期：工程完成後收取合約款項的45%款項。<br>
          第三期：30天執修期後收取餘下5%款項。<br>
        </span>
        <div class="cross-break-prevent signature-container">
          <div class="signature right">
              <div class="signature-header">
                要約接受方客戶或客戶代表
              </div>
              <div class="signature-footer">
              </div>
          </div>
          <div class="signature">
              <div class="signature-header">
                要約提供方受權代表
              </div>
              <div class="signature-footer">
                Fitout Now Limited
              </div>
          </div>
        </div>
        <script type="text/php">
        if ( isset($pdf) ) {
            $font = PDF::getDomPDF()->getFontMetrics()->get_font("genshin", "bold");
            $pdf->page_text($pdf->get_width() - 60, $pdf->get_height() - 20, "{PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
        }
        </script>
    </body>
</html>
