<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
        <title>Trip</title>
        <link rel="stylesheet" href="{{ asset('css/pdf.css')}}">
    </head>
    <body>
        <h1 class="text-center">{{ $data['trip']['name'] }}</h1>
        @set('eachPageDayShow', 4)
        @set('pageOffset', 0)
        @for($i = 0; $i < count($data['days']) / ($eachPageDayShow); $i++)
            <!-- trip summary -->
            <table class="summary cross-break-prevent">
                <thead>
                    <tr>
                        <td style="width: 0">{{ trans('planner.date') }}</td>
                        @for($j=0; $j < $eachPageDayShow ; $j++)
                            @set('dayOffset', $j+$pageOffset)
                            @if($dayOffset < count($data['days']))
                                @set('day', $data['days'][$dayOffset])
                                <td style="width:160px">
                                    @if(empty($day['date_day']))
                                        Day {{ $day['sort'] + 1 }}
                                    @else
                                        <date>{{ $day['date_day'] }} {{ $day['date_month'] }} {{ $day['date_year'] }}</date>
                                    @endif
                                </td>
                            @endif
                        @endfor
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ trans('planner.country') }}</td>
                        @for($j=0; $j < $eachPageDayShow ; $j++)
                            @set('dayOffset', $j+$pageOffset)
                            @if($dayOffset < count($data['days']))
                                @set('day', $data['days'][$dayOffset])
                                <td>
                                    @foreach($day['countries'] as $country)
                                        <div>{{ $country }}</div>
                                    @endforeach
                                </td>
                            @endif
                        @endfor
                    </tr>
                    <tr>
                        <td>{{ trans('planner.transportation') }}</td>
                        @for($j=0; $j < $eachPageDayShow ; $j++)
                            @set('dayOffset', $j+$pageOffset)
                            @if($dayOffset < count($data['days']))
                                @set('day', $data['days'][$dayOffset])
                                <td>
                                    @foreach($day['transportations'] as $transportation)
                                        <div>
                                            {{ $transportation['time'] }} <br>
                                            {{ trans('planner.' . $transportation['direction']) }} - {{ $transportation['place_name'] }}
                                        </div>
                                    @endforeach
                                </td>
                            @endif
                        @endfor
                    </tr>
                    <tr>
                        <td>{{ trans('planner.accommodation') }}</td>
                        @for($j=0; $j < $eachPageDayShow ; $j++)
                            @set('dayOffset', $j+$pageOffset)
                            @if($dayOffset < count($data['days']))
                                @set('day', $data['days'][$dayOffset])
                                <td>
                                    @foreach($day['accommodations'] as $accommodation)
                                        @if(!$accommodation['is_last_day'] && $accommodation['status'] == 'confirmed')
                                            <div>{{$accommodation['display_name']}}</div>
                                        @endif
                                    @endforeach
                                </td>
                            @endif

                        @endfor
                    </tr>
                    <tr>
                        <td>{{ trans('planner.events') }}</td>
                        @for($j=0; $j < $eachPageDayShow ; $j++)
                            @set('dayOffset', $j+$pageOffset)
                            @if($dayOffset < count($data['days']))
                                @set('day', $data['days'][$dayOffset])
                                <td>
                                    <ol>
                                        @foreach($day['events'] as $event)
                                            <li>{{$event['name']}}</li>
                                        @endforeach
                                    </ol>
                                </td>
                            @endif
                        @endfor
                    </tr>

                </tbody>
            </table>
            @set('pageOffset', ($pageOffset + $eachPageDayShow))
        @endfor
        <div class="break"></div>

        <div>
            @foreach($data['days'] as $day)
                @set('dayHasDetails', false)
                {{-- <div class="cross-break-prevent"> --}}
                    <div class="day">
                        <h2>Day {{ $day['sort'] + 1 }} - <small>{{ $day['date_day'] }} {{ $day['date_month'] }} {{ $day['date_year'] }}</small></h2>
                        <p>
                        @set('i', 0)
                        @foreach($day['events'] as $index => $event)
                            {{$i != 0 ? '→' : ''}}  {{ $event['name'] }}
                            @set('i', $i++)
                        @endforeach
                        </p>
                        @if(!empty($day['accommodations']))
                        @foreach($day['accommodations'] as $accommodation)
                            @if($accommodation['is_first_day'] && $accommodation['status'] == 'confirmed')
                                @set('dayHasDetails', true)
                                <h3>{{ trans('planner.accommodation') }}</h3>
                                <div class="list-table">
                                    <div class="row">
                                        <div class="title">{{ trans('planner.name') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['display_name']) ? $accommodation['display_name'] : '&nbsp;' }}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.checkin-date') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['checkin_date']) ? $accommodation['checkin_date'] : '&nbsp;' }}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.checkin-time') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['checkin_time']) ? $accommodation['checkin_time'] : '&nbsp;' }}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.checkout-date') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['checkout_date']) ? $accommodation['checkout_date'] : '&nbsp;' }}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.checkout-time') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['checkout_time']) ? $accommodation['checkout_time'] : '&nbsp;' }}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.address') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['address']) ? $accommodation['address'] : '&nbsp;' }}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.book-via') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['data']['attributes']['book_via']) ? $accommodation['data']['attributes']['book_via'] : '&nbsp;'}}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.booking-no') }}</div>
                                        <div class="content">
                                            {{ !empty($accommodation['data']['attributes']['booking_no']) ? $accommodation['data']['attributes']['booking_no'] : '&nbsp;'}}
                                        </div>
                                    </tr>
                                    <div class="row">
                                        <div class="title">{{ trans('planner.note') }}</div>
                                        <div class="content">
                                            {!! !empty($accommodation['note']) ? nl2br($accommodation['note']) : '&nbsp;' !!}
                                        </div>
                                    </tr>
                                </table>
                            @endif
                        @endforeach
                        @endif
                        @if(!empty($day['events']))
                            <h3>{{ trans('planner.itinerary')}}</h3>
                            <div class="day-event-list-table">
                                @foreach($day['events'] as $event)
                                    @set('dayHasDetails', true)
                                    @if(!$event['attributes']['is_generated'])
                                        <div class="row">
                                                <div class="time">
                                                    {{$event['start_time']}} - {{$event['end_time']}}
                                                </div>
                                                <div class="content">
                                                    <strong>{{ $event['name'] }}</strong>
                                                    <p>{{$event['address']}}</p>
                                                    <p>{!! nl2br($event['note']) !!}</p>
                                                    <br>
                                                </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            {{--<img class="map-img" src="{{$event['map_url']}}" alt="">--}}
                            {{-- <div class="test-img"></div> --}}
                        @else
                            {{ trans('planner.day-no-event') }}
                        @endif
                    </div>
                {{-- </div> --}}
                @if($dayHasDetails)
                    <div class="break"></div>
                @endif
            @endforeach
        </div>
        <div class="pdf-note cross-break-prevent">
            <h2 class="text-center">{{ trans('planner.note') }}</h2>
            @for($i=0; $i < 30; $i++)
                <div class="line"></div>
            @endfor
        </div>
        <script type="text/php">
        if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("genshin", "bold");
            $pdf->page_text($pdf->get_width() - 60, $pdf->get_height() - 20, "{PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
        }
        </script>
    </body>
</html>
