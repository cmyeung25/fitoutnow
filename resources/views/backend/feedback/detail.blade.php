@extends ('backend.layouts.common')
@section('main')
        <div class="row">
            <div class="col-md-6">
                @embed('Widgets.Layout.box')
                    @section('box-title' , !empty($data['id']) ? "Survey Detail - {$data['name']}" : 'Create new survey' )
                    @section('box-tools')
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    @endsection
                    @section('box-body')
                        @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['email'],
                            'field_id' => '',
                            'field_label' => 'Email',
                            'field_name' => 'email',
                            'field_disabled' => true,
                        ])
                        @endembed
                        @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['name'],
                            'field_id' => '',
                            'field_label' => 'Name',
                            'field_name' => 'name',
                            'field_disabled' => true,
                        ])
                        @endembed
                        @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['comment_type'],
                            'field_id' => '',
                            'field_label' => 'Comment Type',
                            'field_name' => 'comment_type',
                            'field_disabled' => true,
                        ])
                        @endembed
                        @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['created_at'],
                            'field_id' => '',
                            'field_label' => 'Created at',
                            'field_name' => 'created_at',
                            'field_disabled' => true,
                        ])
                        @endembed
                        @embed ('Widgets.FormFields.textarea', [
                            'field_placeholder' => '',
                            'field_value' => $data['comment'],
                            'field_id' => '',
                            'field_label' => 'Comment',
                            'field_name' => 'comment',
                            'field_disabled' => true,
                        ])
                        @endembed
                    @endsection
                @endembed
            </div>
            <div class="col-md-6">
            </div>
        </div>
@endsection

@section('content-script')
@endsection
