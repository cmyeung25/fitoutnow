@extends ('backend.layouts.common')
@section('content-header')
    <h1>Feedback List</h1>
@endsection
@section('main')
    @include ('templates.data-table-listing')
@endsection
