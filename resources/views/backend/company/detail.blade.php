@extends ('backend.layouts.common')
@section('main')
        <div class="row">
            <div class="col-md-6">
                @embed('Widgets.Layout.box')
                    @section('box-title' , !empty($data['id']) ? "Company Detail - {$data['name']}" : 'Create new Company' )
                    @section('box-tools')
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    @endsection
                    @section('box-body')
                      @embed ('Widgets.FormFields.detail-form')
                        @section('fields')
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['name'],
                            'field_id' => '',
                            'field_label' => 'name',
                            'field_name' => 'name',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['address'],
                            'field_id' => '',
                            'field_label' => 'address',
                            'field_name' => 'address',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['website'],
                            'field_id' => '',
                            'field_label' => 'website',
                            'field_name' => 'website',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.textarea', [
                            'field_placeholder' => '',
                            'field_value' => $data['description'],
                            'field_id' => '',
                            'field_label' => 'description',
                            'field_name' => 'description',
                          ])
                          @endembed
                        @endsection
                      @endembed
                    @endsection
                @endembed
            </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            @embed('Widgets.Layout.box')
                @section('box-title' , "Stores" )
                @section('box-tools')
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                @endsection
                @section('box-body')
                  <table class="table">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Timeslot</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($data['assets'] as $key => $value)
                        <tr>
                          <td>{{$value['id']}}</td>
                          <td>{{$value['name']}}</td>
                          <td>{{$value['status']}}</td>
                          <td>
                            @foreach ($value['timeslots'] as $timeslot)
                              {{$timeslot['start_date']}} - {{$timeslot['end_date']}} <br>
                            @endforeach
                          </td>
                          <td>
                            @if($value['status'] == 'active')
                              <a href="#" class="btn btn-danger btn-xs">Inactive</a>
                            @else
                              <a href="#" class="btn btn-success btn-xs">Active</a>
                            @endif
                            <a href="../../asset/detail/{{$value['id']}}" class="btn btn-primary btn-xs">Edit</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <a href="../company-timeslot-import-excel/{{$data['id']}}">Download Timeslot excel</a>
                @endsection
            @endembed
          </div>
          <div class="col-md-6">
            @embed('Widgets.Layout.box')
                @section('box-title' , "Timeslot Update Excel" )
                @section('box-tools')
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                @endsection
                @section('box-body')
                  <form action="../company-timeslot-excel" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="company_id" value="{{$data['id']}}">
                    <label>Upload your excel here:</label>
                    <input type="file" name="import_excel">
                    <br>
                    <button type="submit" name="button" class="btn btn-primary">Upload</button>
                  </form>
                @endsection
            @endembed

          </div>
        </div>
@endsection

@section('content-script')
@endsection
