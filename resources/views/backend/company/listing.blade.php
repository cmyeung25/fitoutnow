@extends ('backend.layouts.common')
@section('content-header')
    <h1>Company List</h1>
@endsection
@section('main')
    @include ('templates.data-table-listing')
@endsection
