@extends ('backend.layouts.common')
@section('main')
    <div class="row">
        <div class="col-md-8">
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => $data['last_name'],
                'field_id' => '',
                'field_label' => '姓氏',
                'field_name' => 'last_name',
                'field_disabled' => true
            ])
            @endembed
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => $data['first_name'],
                'field_id' => '',
                'field_label' => '名字',
                'field_name' => 'first_name',
                'field_disabled' => true
            ])
            @endembed
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => $data['email'],
                'field_id' => '',
                'field_label' => '電郵',
                'field_name' => 'email',
                'field_disabled' => true
            ])
            @endembed
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => $data['phone'],
                'field_id' => '',
                'field_label' => '電話',
                'field_name' => 'phone',
                'field_disabled' => true
            ])
            @endembed
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => $data['status'],
                'field_id' => '',
                'field_label' => '狀態',
                'field_name' => 'status',
                'field_disabled' => true
            ])
            @endembed
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => $data['account_retrieved_at'],
                'field_id' => '',
                'field_label' => '帳號提取日期',
                'field_name' => 'account_retrieved_at',
                'field_disabled' => true
            ])
            @endembed
            @if($data['status'] == 'waiting-for-retrieve')
              <label>帳號提取SMS信息</label><br>
              {{-- <div style="border:1px solid lightgrey;padding: 2px;border-radius:2px;background: white;">
                <span> --}}
                <textarea name="sms-content" rows="8" cols="80">{{$data->UserRetrieveSmsContent}}</textarea>
                {{-- </span>
              </div> --}}
              <button class="send-sms">Send SMS</button>
            @endif
        </div>
        <div class="col-md-4">

        </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <h3>Mapped Company</h3>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>ID</th>
              <th>Company Name</th>
              <td>Actions</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($data['company'] as $key => $value)
              <tr>
                <td>{{$value['id']}}</td>
                <td>{{$value['name']}}</td>
                <td><a href="../remove-mapping-company/{{$data['id']}}/{{$value['id']}}" class="btn btn-danger btn-xs">Remove</a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
        <div>
          <form action="../insert-mapping-company" method="post">
            <div class="form-group">
              <label>Insert New Company</label>
              <select name="company_id" class="form-control">
                @foreach ($mappable_companies as $key => $value)
                  <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
              </select>
            </div>
            <input type="hidden" name="user_id" value="{{$data['id']}}">
            <br>
            <button class="btn btn-primary">Insert</button>
          </form>
        </div>
      </div>
    </div>
@endsection
@section('content-script')
  <script>
    $('.send-sms').on('click', function(e){
      e.preventDefault();
      var r = confirm("Are you sure to send out this SMS message?");
      if(r) {
        $.post(siteUrl + '/backend/api/send-user-retrieve-sms-message',{
          'user_id': {{$data->id}},
          'sms_content': $('[name="sms-content"]').val(),
        }).then(function(response){
          console.log(response);
        })
      }
    })
  </script>
@endsection
