@extends ('backend.layouts.common')
@section('main')
    <div class="row">
        <div class="col-md-8">
            <form action="" method="post">

                @embed ('Widgets.FormFields.text', [
                    'field_placeholder' => '',
                    'field_value' => $data['name'],
                    'field_id' => '',
                    'field_label' => 'Name',
                    'field_name' => 'name',
                    // 'field_disabled' => true
                ])
                @endembed
                @embed ('Widgets.FormFields.text', [
                    'field_placeholder' => '',
                    'field_value' => $data['email'],
                    'field_id' => '',
                    'field_label' => 'Email',
                    'field_name' => 'email',
                    // 'field_disabled' => true
                ])
                @endembed
                @embed ('Widgets.FormFields.text', [
                    'field_placeholder' => '',
                    'field_value' => $data['login_type'],
                    'field_id' => '',
                    'field_label' => 'Login Type',
                    'field_name' => 'login_type',
                    'field_disabled' => true
                ])
                <input type="hidden" name="user_id" value="{{$data['id']}}">
                <input type="hidden" name="locale" value="en">
                <button class="btn btn-primary">Update</button>

                @endembed
              </form>
        </div>
        <div class="col-md-4">

        </div>
    </div>
    @if(!empty($data['id']))
      <div class="row">
        <div class="col-md-8">

          @embed('Widgets.Layout.box')
              @section('box-title' , "Change Password")
              @section('box-tools')
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              @endsection
              @section('box-body')
                <form action="../change-password" method="post">
                  <div class="form-group">
                    @embed ('Widgets.FormFields.text', [
                        'field_placeholder' => '',
                        'field_value' => '',
                        'field_id' => 'passowrd',
                        'field_label' => 'Password',
                        'field_name' => 'password',
                        'field_type' => 'password',
                    ])
                    @endembed
                    @embed ('Widgets.FormFields.text', [
                        'field_placeholder' => '',
                        'field_value' => '',
                        'field_id' => 'passowrd-confirm',
                        'field_label' => 'Password Confirm',
                        'field_name' => 'password_confirmation',
                        'field_type' => 'password',
                    ])
                    @endembed
                  </div>
                  <input type="hidden" name="user_id" value="{{$data['id']}}">
                  <br>
                  <button class="btn btn-primary">Insert</button>
                </form>
              @endsection
          @endembed
        </div>
      </div>
    @endif
@endsection
