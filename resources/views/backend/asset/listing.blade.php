@extends ('backend.layouts.common')
@section('content-header')
    <h1>Feedback List</h1>
@endsection
@section('main')
    @include ('templates.data-table-listing')
    <a href="./re-calculate-asset-score" class="btn btn-primary">Recalcuate Score</a>
@endsection
