@extends ('backend.layouts.common')
@section('main')
        <div class="row">
            <div class="col-md-6">
                @embed('Widgets.Layout.box')
                    @section('box-title' , !empty($data['id']) ? "Store Detail - {$data['name']}" : 'Create new store' )
                    @section('box-tools')
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    @endsection
                    @section('box-body')
                      @embed ('Widgets.FormFields.detail-form')
                        @section('fields')
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['name'],
                            'field_id' => '',
                            'field_label' => 'name',
                            'field_name' => 'name',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['address'],
                            'field_id' => '',
                            'field_label' => 'address',
                            'field_name' => 'address',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['min_price'],
                            'field_id' => '',
                            'field_label' => 'min_price',
                            'field_name' => 'min_price',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.select', [
                            'field_placeholder' => '',
                            'field_value' => $data['status'],
                            'field_id' => '',
                            'field_label' => 'status',
                            'field_name' => 'status',
                            'field_options' => [
                              'active' => 'active',
                              'inactive' => 'inactive',
                            ],
                            'field_allow_empty' => false,
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.text', [
                            'field_placeholder' => '',
                            'field_value' => $data['lookup_key'],
                            'field_id' => '',
                            'field_label' => 'lookup_key',
                            'field_name' => 'lookup_key',
                          ])
                          @endembed
                          @embed ('Widgets.FormFields.select', [
                            'field_placeholder' => '',
                            'field_value' => $data['company_id'],
                            'field_id' => '',
                            'field_label' => 'Company',
                            'field_name' => 'company_id',
                            'field_options' => $companiesSelector,
                            'field_allow_empty' => false,
                          ])
                          @endembed

                        @endsection
                      @endembed
                    @endsection
                @endembed
            </div>
            <div class="col-md-6">
            </div>
        </div>
@endsection

@section('content-script')
@endsection
