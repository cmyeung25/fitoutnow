 <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title', 'Dashboard')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <script>
        var siteUrl = '{{ url('/') }}'
    </script>
    @include('backend.partials.style')
    @yield('style')
</head>
<body class="skin-blue fixed" ng-app="App" @yield('body-attr')>
    @yield('content')
    @include('backend.partials.script')
    @yield('post-script')
</body>
</html>
