@extends('backend.layouts.master')
@section('content')
    @include('backend.partials.header')
    <div class="wrapper row-offcanvas row-offcanvas-left">

        @include('backend.partials.sidebar')

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content-header')
                @include('Widgets.Menu.breadcrumb', ['breadcrumb' => $breadcrumb])
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('main')
            </section>
        </aside>
        <!-- /.right-side -->
    </div>
    <!-- ./wrapper -->
    @yield('modal')

    <!-- add new calendar event modal -->

@endsection
@section('post-script')
    @yield('content-script')
@endsection
