@extends ('backend.layouts.common')
@section('main')
  @embed ('Widgets.FormFields.detail-form')
      @section('fields')
        <div class="row">
            <div class="col-md-6">
                @embed('Widgets.Layout.box')
                    @section('box-title' , !empty($data['id']) ? "Mall Detail - {$data['name']}" : 'Create new mall' )
                    @section('box-tools')
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    @endsection
                    @section('box-body')
                      @embed ('Widgets.FormFields.text', [
                          'field_placeholder' => '',
                          'field_value' => $data['name'],
                          'field_id' => '',
                          'field_label' => 'Name',
                          'field_name' => 'name',
                      ])
                      @endembed
                      @embed ('Widgets.FormFields.text', [
                          'field_placeholder' => '',
                          'field_value' => $data['address'],
                          'field_id' => '',
                          'field_label' => 'Address',
                          'field_name' => 'address',
                      ])
                      @endembed
                      <div id="region-picker">
                          <label>地區</label>
                          <div class="form-group select">
                              <select name="district_id" id="district">
                                  <option value="">請選擇地區</option>
                                  @foreach($districts as $district)
                                      <option value="{{ $district->id }}" {{ $data['district_id'] == $district->id ? 'selected' : '' }}>{{ $district->name }}</option>
                                  @endforeach
                              </select>
                              <span class="error-message"></span>
                          </div>
                          <div class="form-group select">
                              <select name="sub_district_id" id="sub-district">
                                  <option value="">請選擇地區</option>
                                  @foreach($subDistricts as $subDistrict)
                                      <option value="{{ $subDistrict->id }}" {{ $data['sub_district_id'] == $subDistrict->id ? 'selected' : '' }}>{{ $subDistrict->name }}</option>
                                  @endforeach
                              </select>
                              <span class="error-message"></span>
                          </div>
                          <br>
                      </div>
                      @embed ('Widgets.FormFields.map-picker', [
                          'field_placeholder' => '',
                          'field_id' => 'mall_map',
                          'field_label' => 'Address',
                          'field_name' => 'address',
                          'field_zoom' => 16,
                          'field_api_include' => false,
                          'field_autocomplete' => true,
                          'field_lat_name' => 'lat',
                          'field_lng_name' => 'lng',
                          'field_lat_value' => $data['lat'],
                          'field_lng_value' => $data['lng'],
                      ])
                      @endembed
                    @endsection
                @endembed

            </div>
            <div class="col-md-6">
            </div>
        </div>
      @endsection
   @endembed
@endsection

@section('content-script')
  <script async defer src="https://maps.googleapis.com/maps/api/js?language=zh-TW&callback=initMap_mall_map&key=AIzaSyCMi1GlANPUcxtITJ6b2lPAI7f93auS-Z4&libraries=places"></script>
  <script>
  var $regionPicker = $('#region-picker');
  var $district = $('#district');
  var $subDistrict = $('#sub-district');
  $district.select2({
      placeholder: "請選擇地區",
      allowClear: true,
      width: '100%',
  });
  $district.on('change', function (e) {
      var districtId = $(this).val();
      var url = siteUrl + '/api/sub-district-by-district-id/' + districtId;
      $subDistrict.select2({ width: '100%' });
      $subDistrict.select2('destroy');
      $subDistrict.hide();
      $subDistrict.find('option').remove().end().append('<option value="">請選擇地區</option>');
      if(districtId != '') {
          $.get(url, function(message){
              message = JSON.parse(message);
              if( message.data.length > 0) {
                  var selectData = [];
                  for (var i = 0; i < message.data.length; i++) {
                      selectData.push({
                          id: message.data[i].id,
                          text: message.data[i].name,
                      })
                  }
                  $subDistrict.select2({
                      data : selectData,
                      placeholder: "請選擇地區",
                      allowClear: true,
                      width: '100%',
                  });
                  $subDistrict.show();
              } else {
                  $subDistrict.hide();
              }
          })
      }
  });
  $subDistrict.select2({ width: '100%' });
  </script>
@endsection
