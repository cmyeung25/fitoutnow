@extends ('backend.layouts.master')
@section('content')
<div class="box box-primary login-box">
    <div class="box-header">
        <h1 class="box-title">Login</h1>
    </div>
    <div class="box-body">
        <form class="ajaxForm" role="form" method="POST" action="{{ $actionUrl }}">
            {!! csrf_field() !!}
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => '',
                'field_id' => '',
                'field_label' => 'Email',
                'field_name' => 'email',
            ])
            @endembed
            @embed ('Widgets.FormFields.text', [
                'field_placeholder' => '',
                'field_value' => '',
                'field_id' => 'passowrd',
                'field_label' => 'Password',
                'field_name' => 'password',
                'field_type' => 'password',
            ])
            @endembed
            @embed ('Widgets.FormFields.checkbox', [
                'field_value' => '1',
                'field_label' => 'Remember me',
                'field_name' => 'remember',
            ])
            @endembed
            <div class="form-group">
                <span class="message error-message"></span>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </form>
    </div>
</div>
@endsection
