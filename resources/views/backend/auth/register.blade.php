@extends ('backend.layouts.master')
@section('content')
<div class="box box-primary login-box">
    <div class="box-header">
        <h1 class="box-title">Register</h1>
    </div>
    <div class="box-body">
        <form class="ajaxForm" role="form" method="POST" action="{{ $actionUrl }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="">Name</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <label for="">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>

            <div class="form-group">
                <label for=""> Confirm Password</label>
                <input class="form-control" type="password" name="password_confirmation">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Register</button>
            </div>
        </form>

    </div>

</div>
@endsection
