@extends ('backend.layouts.common')
@section('content-header')
    <h1>Dashboard</h1>
@endsection
@section('main')
    <div class="row">
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Users Count</span>
           {{-- <span class="info-box-number">{{ $userCount }}</span> --}}
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-red"><i class="fa fa-building"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Asset Count</span>
           {{-- <span class="info-box-number">{{ $assetCount }}</span> --}}
         </div>
       </div>
     </div>
     {{-- <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-green"><i class="fa fa-map-marker"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Trip Event Count</span>
           <span class="info-box-number">{{ $tripEventCount }}</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-yellow"><i class="fa fa-bed"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Trip Accommodation Count</span>
           <span class="info-box-number">{{ $tripAccommodationCount }}</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-navy"><i class="fa fa-bookmark"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Trip Transportation Count</span>
           <span class="info-box-number">{{ $tripTransportationCount }}</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-teal"><i class="fa fa-bookmark"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Trip Bookmarks Count</span>
           <span class="info-box-number">{{ $tripBookmarkCount }}</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-maroon"><i class="fa fa-comment"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Feedback Count</span>
           <span class="info-box-number">{{ $tripFeedbackCount }}</span>
         </div>
       </div>
     </div> --}}


   </div>
@endsection
