import {
  LOAD_QUESTION_BANK,
  LOAD_FLAT_BANK,
  SAVE_QUOTATION,
  LOAD_QUOTATION,
  SUBMIT_QUOTATION,
  CLEAR_QUOTATION,
} from "../constants/action-types";

export function loadQuestionBank(payload) {
  return { type: LOAD_QUESTION_BANK, payload }
};

export function loadFlatBank(payload) {
  return { type: LOAD_FLAT_BANK, payload }
};

export function saveQuotation(payload) {
  return { type: SAVE_QUOTATION, payload }
};

export function loadQuotation(payload) {
  return { type: LOAD_QUOTATION, payload }
};
export function submitQuotation(payload) {
  return { type: SUBMIT_QUOTATION, payload }
};
export function clearQuotation(payload) {
  return { type: CLEAR_QUOTATION, payload }
};
