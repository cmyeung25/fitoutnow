import React from 'react';
import ReactDOM from 'react-dom';
import store from '../../store/index';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { clearQuotation } from '../../actions/index';

class InitCalculator extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    
  }
  _goToCalculator = () => {
    this.props.history.push('/calculator')
  }
  _clearQuotation = () => {
    this.props.clearQuotation();
    this._goToCalculator();
  }

  render() {
    if(this.props.quotationExist != true) {
      this._goToCalculator()
    }

    if(this.props.quotationExist == true) {
      return (
          <div className="container">
             <div className="get-started">
                <div className="row">
                  <div className="col-6">
                      <div className="selection" onClick={()=>this._goToCalculator()}>
                        <span>繼續現有報價</span>
                      </div>
                  </div>
                  <div className="col-6">
                    <div className="selection" onClick={()=>this._clearQuotation()}>
                      <span>重新報價</span>
                    </div>
                  </div>
                </div>
             </div>
          </div>
      );
    } else {
      return null;
    }
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    // addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    clearQuotation: payload => dispatch(clearQuotation(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    // currentCurrency: state.currentCurrency,
    // availableCurrencies: state.availableCurrencies
    quotation: state.quotation,
    quotationExist: state.quotationExist
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InitCalculator));

// if (document.getElementById('example')) {
//     ReactDOM.render(<InitCalculator />, document.getElementById('example'));
// }
