import React from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loadQuestionBank,loadFlatBank,loadQuotation,saveQuotation } from '../../actions/index';
import { all, create } from 'mathjs'

import jsonData from '../../../json/questions.json';
import flatData from '../../../json/flats.json';

const math = create(all, {})
const parser = math.parser()
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const regions = [
  "香港島",
  "九龍",
  "新界",
]
const districts = [
  {"name":"灣仔",region:"香港島"},
  {"name":"銅鑼灣",region:"香港島"},
  {"name":"旺角",region:"九龍"},
  {"name":"元朗",region:"新界"},
  {"name":"屯門",region:"新界"},
]

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props.quotation,
      token: null,
      region: null,
      district: null,
      initializing: true,
      isDebugShow:false,
    };
  }

  componentDidMount () {
    this._composeData()
    this._setupMathParser();
    this._loadExisting();
  }

  _composeData = () => {
    var nestedData = {
      categories:[],
    };

    for (var categoryId in jsonData.categories) {
      var category = {...jsonData.categories[categoryId]};
      var questions = [];
      for (var questionId in jsonData.questions) {
        if(jsonData.questions[questionId].sectionId == category.sectionId){
          var question = {...jsonData.questions[questionId]};
          var answers = [];
          for(var answerId in jsonData.answers) {
            if(jsonData.answers[answerId].questionId == question.questionId) {
              var answer = {...jsonData.answers[answerId]};
              switch(answer.answerType) {
                case "answer":
                  answer.answerOption = this._getAnswerOption(answer.answerValue)
                  break;
                case "question":
                  // answer.answerQuestion = this._getQuestion(answer.answerValue)
                  // console.log(answer.answerValue)
                  break;
                case "select_option":
                  var selectOptions = [];
                  for (var selectOptionId in jsonData.optionListOptions) {
                    if(jsonData.optionListOptions[selectOptionId].groupKey == answer.answerValue) {
                      var selectOption = jsonData.optionListOptions[selectOptionId];
                      selectOption.detail = this._getAnswerOption(selectOption.optionId);
                      selectOptions.push(selectOption);
                    }
                  }
                  if(selectOptions.length == 0) {
                    console.error('Answer "' + answer.title + '" in ' + question.questionId + " missing options")
                  }
                  answer.selectOptions = selectOptions;
                  break;
              }
              answers.push(answer);
            }
          }
          question.answers = answers;
          question.category = {...jsonData.categories[categoryId]};
          questions.push(question);
        }
      }
      category.questions = questions;
      nestedData.categories.push(category)
    }

    this.props.loadQuestionBank({data:nestedData})
    this.props.loadFlatBank({data:flatData.data})
  }
  _setupMathParser = () => {
    for (var i = 0; i < flatData.attributes.length; i++) {
      var attr = flatData.attributes[i];
      if(this.state.flat != null) {
        parser.set("$"+attr, this.state.flat[attr])
      } else {
        parser.set("$"+attr, 1)
      }
    }
    parser.set("$total", 0)

  }
  _loadExisting = () => {
    if(this.props.location.pathname == '/modify') {
      var token = this._findGetParameter('token');

      const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            token:token
          })
      };
      fetch('/api/get-quotation', requestOptions)
          .then(response => response.json())
          .then(data => {
            this.setState({
              ...data.data.quotation.content.quotation,
              token:token,
              initializing:false
            })
          });
    } else {
      this.setState({initializing:false})
    }
  }

  _findGetParameter = (parameterName) => {
      var result = null,
          tmp = [];
      this.props.location.search
          .substr(1)
          .split("&")
          .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
          });
      return result;
  }

  _updateContactInfo = (attribute, value) => {
    var state = {...this.state}

    state.contactInfo[attribute] = value;

    this.setState(state)
  }



  _getAnswerOption = (key) => {
    for (var optionId in jsonData.options) {
      if(jsonData.options[optionId].key == key) {
        return jsonData.options[optionId];
      }
    }
    console.error("missing answer option " + key)
    return null;
  }

  _getQuestion = (key) => {
    for (var questionId in jsonData.questions) {
      if(jsonData.questions[questionId].questionId == questionId) {
        return jsonData.questions[questionId];
      }
    }
  }

  _calculateTotal = (answers) => {
    var total = 0;
    var afterCalcAnsIndex = [];

    for (var i in answers) {
      if(answers[i].isMultple == true && answers[i].valid != false) {
        for (var j in answers[i].answers) {
          if(answers[i].answers[j].isCalcAfter) {
            //todo
            // afterCalcAns.push(answers[i].answers[j])
          } else {
            if(typeof answers[i].answers[j].cost != 'undefined') {
              total += answers[i].answers[j].cost
            }
          }
        }
      } else {
        if(answers[i].answer.isCalcAfter) {
          afterCalcAnsIndex.push(i)
        } else {
          if(typeof answers[i].answer != 'undefined' && typeof answers[i].answer.cost != 'undefined' && answers[i].valid != false) {
            total += answers[i].answer.cost
          }
        }

      }
    }
    parser.set("$total", total)
    for (var i in afterCalcAnsIndex) {
      var answer = answers[afterCalcAnsIndex[i]].answer;
      var cost = this._calcAnswerCost(answer,true,true);
      answer.cost = cost.cost;
      answer.unit = cost.unit;
      answer.unitPrice = cost.unitPrice;
      answer.isCalcAfter = true;
      total = total + answer.cost;
      console.log(total, answer.cost);
    }

    return {
      total:total,
      answers:answers
    }
  }

  _selectFlat = (flatContent) => {
    this.setState({
      flat:flatContent,
      bedroomCount: this._calcFlatBedroomCount(flatContent),
    })
    setTimeout(()=>{
      this._setupMathParser();
    },100)
  }

  _calcFlatBedroomCount = (flatContent) => {
    if(flatContent.rm_1_ce == 0) {
      return 0
    }
    if(flatContent.rm_2_ce == 0) {
      return 1
    }
    if(flatContent.rm_3_ce == 0) {
      return 2
    }
    return 3;
  }

  _goToQuestionList = (roomContent, roomIndex) => {
    window.scrollTo(0, 0)
    this.setState({
      view:"room-question-list-screen",
      roomIndex:roomIndex,
      roomContent:roomContent
    })
  }

  _goToFlatSelectionScreen = () => {
    window.scrollTo(0, 0)
    this.setState({
      view:"flat-selection-screen",
      roomIndex:0,
      roomContent:null
    })
  }

  _goToContactInfoScreen = () => {
    window.scrollTo(0, 0)
    this.setState({
      view:"contact-info-screen",
      roomIndex:0,
      roomContent:null
    })
  }

  _goToRoomSelectionScreen = () => {
    window.scrollTo(0, 0)
    this.setState({
      view:"room-selection-screen",
      roomIndex:0,
      roomContent:null
    })
  }

  _goToBasicQuestionScreen = () => {
    window.scrollTo(0, 0)
    this.setState({
      view:"basic-question-screen",
      roomIndex:0,
      roomContent:null
    })
  }



  _goToQuotationConfirmationScreen = () => {
    this.props.saveQuotation({data:this.state});
    this.props.history.push('/review')
  }

  _answerQuestion = (question, questionId, answer) => {
    var answers = {...this.state.answers}
    var answer = {...answer};
    answer.roomIndex = this.state.roomIndex;

    var cost = this._calcAnswerCost(answer);
    answer.cost = cost.cost;
    answer.unit = cost.unit;
    answer.unitPrice = cost.unitPrice;
    answer.isCalcAfter = cost.isCalcAfter;
    answer.valid = true;

    answers[questionId] = {
      question:question,
      answer:answer,
      isMultple:false,
    };

    answers = this._checkIfAnswerValid(answers)

    var total = this._calculateTotal(answers)
    this.setState({
      answers:total.answers,
      total:total.total,
    })
    this.props.saveQuotation({data:this.state});

  }
  _answerQuestionOption = (question, questionId, answer, selectedOption) => {
    var answers = {...this.state.answers}

    answer.selectedOption = selectedOption;
    answer.roomIndex = this.state.roomIndex;

    var cost = this._calcAnswerCost(answer);
    answer.cost = cost.cost;
    answer.unit = cost.unit;
    answer.unitPrice = cost.unitPrice;
    answer.isCalcAfter = cost.isCalcAfter;
    answer.valid = true;

    answers[questionId]= {
      question:question,
      answer:answer,
      isMultple:false,
    };

    // answers = this._checkIfAnswerValid(answers)

    var total = this._calculateTotal(answers)
    this.setState({
      answers:total.answers,
      total:total.total,
    })
  }

  _answerQuestionInput = (question, questionId, answer, value) => {
    var answers = {...this.state.answers}
    answer = {...answer};
    answer.inputValue = value;
    answer.roomIndex = this.state.roomIndex;

    var cost = this._calcAnswerCost(answer);
    answer.cost = cost.cost;
    answer.unit = cost.unit;
    answer.unitPrice = cost.unitPrice;
    answer.isCalcAfter = cost.isCalcAfter;
    answer.valid = true;

    answers[questionId]= {
      answer:answer,
      question:question,
      isMultple:false,
    };

    // answers = this._checkIfAnswerValid(answers)

    var total = this._calculateTotal(answers)
    this.setState({
      answers:total.answers,
      total:total.total,
    })
  }

  _answerQuestionInputMulti = (question, questionId,answer,value) => {
    var answers = {...this.state.answers}
    answer = {...answer};

    var data = {};

    if(typeof answers[questionId] == "undefined") {
      data.isMultple = true;
      data.roomIndex = this.state.roomIndex;
      data.question = question;
      data.answers = {};
    } else {
      data = answers[questionId];
    }

    data.answers[answer.answerId] = answer;
    data.answers[answer.answerId].inputValue = value;

    var cost = this._calcAnswerCost(data.answers[answer.answerId]);
    answer.cost = cost.cost;
    answer.unit = cost.unit;
    answer.unitPrice = cost.unitPrice;
    answer.isCalcAfter = cost.isCalcAfter;
    answer.valid = true;

    answers[questionId] = data;

    // answers = this._checkIfAnswerValid(answers)

    var total = this._calculateTotal(answers)
    this.setState({
      answers:total.answers,
      total:total.total,
    })
  }

  _checkIfAnswerValid = (answers) => {
    for (var id in answers) {
      if(answers[id].question.disabled == 1) {
        answers[id].valid = this._checkIfQuestionShow(id, answers)
      }
    }
    return answers;
  }
  _checkIfQuestionShow = (questionId, answers) => {
    if(answers == null) {
      answers = this.state.answers;
    }
    for (var id in answers) {
      var answerData = answers[id];

      if(typeof answerData.answer != "undefined" && answerData.answer.answerType == "question" && answerData.answer.answerValue == questionId) {
        return true
      }
    }
    return false;
  }

  _getMultiInputAnswer = (questionId, answer) => {
    if(typeof this.state.answers[questionId] == "undefined") {
      return null
    }
    if(typeof this.state.answers[questionId].answers == "undefined") {
      return null
    }
    if(typeof this.state.answers[questionId].answers == "undefined") {
      return null
    }
    if(typeof this.state.answers[questionId].answers[answer.answerId] == "undefined") {
      return null
    }

    return this.state.answers[questionId].answers[answer.answerId].inputValue
  }


  _calcAnswerCost = (answer, isAfter = false, isIgnoreOptionValue = false) => {
    var unitPrice = 0;
    var unit = 0;
    var cost = 0;
    var isCalcAfter = false;
    var inputValue = answer.hasInput? 0: 1;

    if(typeof answer.inputValue != "undefined") {
      inputValue = answer.inputValue;
    }

    var formula = answer.formula;
    if(typeof formula == "string" || typeof formula == "String") {
      formula = formula.replace("[i]",answer.roomIndex+1);
    }

    if(answer.answerType == "answer") {
      var optionPrice = typeof answer.answerOption.price =="undefined" ? 0 : answer.answerOption.price;
      if(isIgnoreOptionValue) {
        optionPrice = 1;
      }

      if(typeof answer.formula == "undefined") {
        cost = optionPrice * inputValue;
        unit = inputValue;
        unitPrice = optionPrice
      } else {
        cost = parser.evaluate(formula) * optionPrice * inputValue;
        unit = parser.evaluate(formula) * inputValue;
        unitPrice = optionPrice
      }
    } else if(answer.answerType == "select_option") {
      var selectedOptionPrice = typeof answer.selectedOption == "undefined" ? 0 : answer.selectedOption.detail.price;
      if(typeof answer.formula == "undefined") {
        cost = selectedOptionPrice * inputValue;
        unit = inputValue;
        unitPrice = selectedOptionPrice
      } else {
        cost = parser.evaluate(formula) * selectedOptionPrice * inputValue;
        unit = parser.evaluate(formula) * inputValue;
        unitPrice = selectedOptionPrice
      }
    }

    var hasTotalFormula = false;

    if(typeof formula != 'undefined' && formula.includes('$total')) {
      hasTotalFormula = true;
    }

    if(typeof formula != 'undefined' && formula.includes('$total') && !isAfter){
      isCalcAfter = true;
    }

    return {
      cost:!isCalcAfter ? cost: 0,
      unit: hasTotalFormula ? unitPrice : unit,
      unitPrice: hasTotalFormula ? unit : unitPrice,
      isCalcAfter: isCalcAfter,
    }
  }



  _createQuestionView = (question, qIndex) => {
    var questionId = question.questionId;
    if(this.state.roomIndex == 1) {
      questionId = question.questionId + "b"
    }
    if(this.state.roomIndex == 2) {
      questionId = question.questionId + "c"
    }

    if(this._checkIfQuestionShow(questionId) || question.disabled == 0) {
      return (
        <div className="question-view">
          <span>{questionId} - {question.title}</span>
          <div className="question-answers">
            {question.answers.map((answer, aIndex)=>{
              var isSelected = false;
              var isShow = true;
              //answer type -single
              if(!question.isMultipleAns && typeof this.state.answers[questionId] != "undefined" && this.state.answers[questionId].answer.answerId ==  answer.answerId) {
                isSelected = true;
              }

              if(question.isMultipleAns) {
                var isShow = false;
              }

              var className = isSelected ? "btn btn-primary" : 'btn btn-outline-dark';

              if(isShow) {
                return (
                  <button className={className} key={"question-answers-"+questionId+"_"+aIndex} onClick={()=>this._answerQuestion(question,questionId,answer)}>{answer.title}</button>
                )
              }
            })}
            {/* answer /w input (single answer)*/}
            {typeof this.state.answers[questionId] != "undefined" && question.isMultipleAns == 0 && this.state.answers[questionId].answer.hasInput == 1 && (
              <div className="question-answer-input">
                <div>
                  <input type="text" value={this.state.answers[questionId].answer.inputValue} onChange={(event)=>this._answerQuestionInput(question,questionId,this.state.answers[questionId].answer,event.target.value)}/>
                  {typeof this.state.answers[questionId].answer.inputUnit != "undefined" && (
                    <span>{this.state.answers[questionId].answer.inputUnit}</span>
                  )}
                </div>
                <div>
                  {this.state.answers[questionId].requiredPMCheck == 1 && (
                      <span className="input-alert-message">*準確的金額待我們工程負責人現場爲你預算</span>
                  )}
                </div>
              </div>
            )}
            {/* question type "select_option" view  */}
            {typeof this.state.answers[questionId] != "undefined" && question.isMultipleAns == 0 &&this.state.answers[questionId].answer.answerType == "select_option" && (
              <div className="question-answer-options">
                {this.state.answers[questionId].answer.selectOptions.map((option,oIndex) => {
                    var isSelected = false;
                    if(typeof this.state.answers[questionId].answer.selectedOption != "undefined" && this.state.answers[questionId].answer.selectedOption.optionId ==  option.optionId) {
                      isSelected = true;
                    }
                    var className = isSelected ? "btn btn-primary" : 'btn btn-outline-dark';
                    var unit = typeof this.state.answers[questionId].answer.inputUnit != "undefined" && this.state.answers[questionId].answer.inputUnit != "" ? " / " + this.state.answers[questionId].answer.inputUnit : "";
                    return (
                      <button className={className} onClick={()=>this._answerQuestionOption(question,questionId,this.state.answers[questionId].answer,option)}>{option.title} [+HKD {new Intl.NumberFormat().format(option.detail.price)}{unit}]</button>
                    )
                })}
              </div>
            )}
            {/* answer /w input (multi answer)*/}
            {question.isMultipleAns == 1 && question.answers.map((answer, aIndex)=>{
              if(answer.hasInput) {
                return (
                  <div key={"question-input-answers-"+questionId+"_"+aIndex}>
                    <span>{answer.title}</span>
                    {/* write a function to get inputvalue from answers stack  */}
                    <input type="text" value={this._getMultiInputAnswer(questionId, answer)} onChange={(event)=>this._answerQuestionInputMulti(question,questionId,answer,event.target.value)}/>
                    {typeof answer.inputUnit != "undefined" && (
                      <span>{answer.inputUnit}</span>
                    )}
                  </div>
                )
              } else {
                return (
                  <div key={"question-input-answers-"+questionId+"_"+aIndex}>
                    <span>{answer.title}</span>
                    {/* write a function to get inputvalue from answers stack  */}
                    <input type="checkbox" value="1" checked={this._getMultiInputAnswer(questionId, answer) == 1} onChange={(event)=>
                      this._answerQuestionInputMulti(question,questionId,answer,event.target.checked ? 1 : 0)
                    }/>
                  </div>
                )
                // <input type="text" value={this._getMultiInputAnswer(questionId, answer)} onChange={(event)=>this._answerQuestionInputMulti(question,questionId,answer,event.target.value)}/>
              }
            })}
          </div>
        </div>
      )
    }
  }

  _getFooter = () => {
    var actionText = "";
    var actionFunction = null;
    var nextBtnDisabled = true;
    switch (this.state.view) {
      case "flat-selection-screen":
        actionText = (<span>下一步 <i className="fa fa-angle-right"></i></span>)
        nextBtnDisabled = this.state.flat == null;
        actionFunction = () => this._goToContactInfoScreen()
        break;
      case "contact-info-screen":
        actionText = (<span>下一步 <i className="fa fa-angle-right"></i></span>)
        nextBtnDisabled = this.state.contactInfo.title == "" ||
                          this.state.contactInfo.phone == "" ||
                          this.state.contactInfo.email == "" ||
                          this.state.contactInfo.name == "" ||
                          emailRegex.test(this.state.contactInfo.email) == false

        actionFunction = () => this._goToRoomSelectionScreen()
        break;
      case "room-selection-screen":
        actionText = (<span>下一步 <i className="fa fa-angle-right"></i></span>)
        actionFunction = () => this._goToBasicQuestionScreen()
        nextBtnDisabled = false;
        break;
      case "room-question-list-screen":
        actionText = (<span>返回 <i className="fa fa-angle-left"></i></span>)
        actionFunction = () => this._goToRoomSelectionScreen()
        nextBtnDisabled = false;
        break;
      case "basic-question-screen":
        actionText = (<span>確認 <i className="fa fa-angle-right"></i></span>)
        actionFunction = () => this._goToQuotationConfirmationScreen()
        nextBtnDisabled = false;
      default:
    }


    return (
      <div className="calculator-footer">
        <div className="footer-left">
          <span className="footer-left-desc">總計</span>
          <span className="footer-subtotal">HKD {new Intl.NumberFormat().format(this.state.total)}</span>
        </div>
        <div className="footer-right">
          <button onClick={actionFunction} disabled={nextBtnDisabled}>
            {actionText}
          </button>
        </div>
      </div>
    )
  }

  _toggleDebugPanel = () => {
    this.setState({isDebugShow:!this.state.isDebugShow})
  }

  render() {
    var debugResults = [];
    // console.log(this.state.answers)
    for (var questionId in this.state.answers) {
      var singleAns;
      var multipleAnsData = [];
      var multipleAns;

      if(this.state.answers[questionId].isMultple == false) {
        singleAns = (
          <div>
            <span>Answer: {this.state.answers[questionId].answer.title}({this.state.answers[questionId].answer.answerValue})</span><br/>
            {typeof this.state.answers[questionId].answer.selectedOption != "undefined" && (
              <span>Selected Option: {this.state.answers[questionId].answer.selectedOption.title}({this.state.answers[questionId].answer.selectedOption.optionId})</span>
              )
            }
            {typeof this.state.answers[questionId].answer.cost != "undefined" && (
              <span>Cost: HKD{new Intl.NumberFormat().format(this.state.answers[questionId].answer.cost)}</span>
              )
            }
            {this.state.answers[questionId].valid == false && (
              <span> [No included] </span>
            )}
          </div>
        );
      }

      if(this.state.answers[questionId].isMultple == true) {
        for (var i in this.state.answers[questionId].answers) {
          var answer = this.state.answers[questionId].answers[i];
          var inputData = (
            <div>
              <span>Answer: {answer.title} ({answer.answerValue})</span><br/>
              {typeof answer.cost != "undefined" && (
                <span>Cost: HKD{new Intl.NumberFormat().format(answer.cost)}</span>
                )
              }
            </div>
          )
          multipleAnsData.push(inputData)
        }
        multipleAns = (
          <div>
            {multipleAnsData}
          </div>
        );
      }

      debugResults.push((
        <div>
          <b>{questionId} {this.state.answers[questionId].isMultple == true && "Mulitple"}</b> <br/>
          {this.state.answers[questionId].isMultple == false && (
            <div>
              {singleAns}
            </div>
          )}
          {this.state.answers[questionId].isMultple == true && (
            <div>
              {multipleAns}
            </div>
          )}
        </div>
      ))
    }

    if(this.state.initializing) {
      return null;
    }
    var debugClass = "debug-result";
    if(this.state.isDebugShow) {
      debugClass = debugClass + " show"
    }
    return (
      <div className="calculator">
        <div className="debug-toggle" onClick={()=>{this._toggleDebugPanel()}}>!</div>
        <div className={debugClass}>
           {debugResults}
        </div>
        <div className="container">
          {this.state.flat != null && (
            <h2>單位: {this.state.flat.description}</h2>
          )}
          {this.state.view == "flat-selection-screen" && (
            <div className="flat-selection-screen">
              <h1>選擇區域</h1>
              <div className="row">
                {regions.map((region, index) => {
                  return(
                        <button className="flat-option btn btn-outline-dark" key={"region-option-"+index} onClick={()=> this.setState({region:region})}>
                            <span>{region}</span>
                        </button>
                  )
                })}
              </div>
              <br/>
              {this.state.region != null &&
                (
                  <div>
                    <h1>選擇地區</h1>
                    <div className="row">
                    {districts.map((district, index) => {
                      if(district.region == this.state.region) {
                        return (
                            <button className="flat-option btn btn-outline-dark" key={"district-option-"+index} onClick={()=> this.setState({district:district.name})}>
                                <span>{district.name}</span>
                            </button>
                        )
                      }
                    })}
                    </div>
                  </div>
                )
              }
              <br/>
              {this.state.region != null && this.state.district != null && (
                <div>
                  <h1>選擇單位</h1>
                  <div className="row">
                  {this.props.flatBank !=null && this.props.flatBank.map((value, index) => {
                    if(value.district == this.state.district && value.region == this.state.region) {
                      return(
                            <button className="flat-option btn btn-outline-dark" key={"flat-option-"+index} onClick={()=> this._selectFlat(value)}>
                                <span>{value.description}</span>
                            </button>
                      )
                    }
                    })
                  }
                  </div>
                </div>
              )}


            </div>
          )}
          {this.state.view == "contact-info-screen" && (
            <div className="contact-info-screen">
              <h1>聯絡方式</h1>
              <div className="contact-info">
                <div className="form-group">
                  <label>稱謂</label><br/>
                  <div className="input-button-group">
                    {this.state.options.title.map((value, index) => {
                      var isSelected = false;
                      if(this.state.contactInfo.title == value) {
                        isSelected = true;
                      }

                      var className = isSelected ? "btn btn-primary" : 'btn btn-outline-dark';
                      return (
                        <button className={className} key={"contact-info-title-"+index} onClick={()=>this._updateContactInfo("title",value)}>{value}</button>
                      )
                    })}
                  </div>
                </div>
                <div className="form-group">
                  <label>姓名</label><br/>
                  <input className="form-control" type="text" value={this.state.contactInfo.name} onChange={(event)=>this._updateContactInfo("name",event.target.value)}/>
                </div>
                <div className="form-group">
                  <label>電話</label><br/>
                  <input className="form-control" type="text" value={this.state.contactInfo.phone} onChange={(event)=>this._updateContactInfo("phone",event.target.value)}/>
                </div>
                <div className="form-group">
                  <label>電郵</label><br/>
                  <input className="form-control" type="text" value={this.state.contactInfo.email} onChange={(event)=>this._updateContactInfo("email",event.target.value)}
                    onFocus={()=>{this.setState({emailError:false})}} onBlur={()=>{this.setState({emailError:!emailRegex.test(this.state.contactInfo.email)})}}/>
                    {this.state.emailError && (
                      <span className="error-message">請輸入正確電郵地址</span>
                    )}
                </div>
              </div>
            </div>
          )}


          {this.state.view == "room-selection-screen" && (
            <div className="room-selection-screen">
              <span className="back-btn" onClick={()=>this._goToFlatSelectionScreen()}>
                <i className="fa fa-chevron-left"></i> 返回
              </span>
              <h3>請選擇房間</h3>
              <div className="row">
                {this.props.questionBank!= null && this.props.questionBank.categories.map((value, index) => {
                  switch (value.key) {
                    case "living":
                    case "kitchen":
                    case "bathroom":
                      return (
                        <div className="col-md-6" key={"room-type-"+index} onClick={() => this._goToQuestionList(value,0)}>
                          <div className="room-type">
                            <span>{value.title}</span>
                          </div>
                        </div>
                      )
                      break;
                    case "bedroom":
                      var content = [];
                      for (var i = 0; i < this.state.bedroomCount; i++) {
                        content.push(
                          <div className="col-md-6" key={"room-type-"+index+"-"+i} onClick={this._goToQuestionList.bind(this, value, i)}>
                            <div className="room-type">
                              <span>{value.title} {i+1}</span>
                            </div>
                          </div>
                        )
                      }
                      return content;
                      break;
                    default:

                  }
                })}
              </div>
            </div>
          )}
          {this.state.view == "room-question-list-screen" && (
            <div className="room-question-list-screen">
              <span className="back-btn" onClick={()=>this._goToRoomSelectionScreen()}>
                <i className="fa fa-chevron-left"></i> 返回
              </span>
              <h1>{this.state.roomContent.title}</h1>
              {this.props.questionBank!= null && this.state.roomContent.questions.map((question, qIndex) => {
                return this._createQuestionView(question,qIndex);
              })}
              <div className="row">
                <div className="col-md-3">
                  <button className="btn btn-primary " onClick={()=>this._goToRoomSelectionScreen()}>
                    <i className="fa fa-chevron-left"></i> 返回
                  </button>
                </div>
              </div>
            </div>
          )}
          {this.state.view == "basic-question-screen" && (
            <div className="basic-question-screen">
              <span className="back-btn" onClick={()=>this._goToRoomSelectionScreen()}>
                <i className="fa fa-chevron-left"></i> 返回
              </span>
              {/* basic question */}
                <h1>基本資料</h1>
                {this.props.questionBank!= null && this.props.questionBank.categories.map((value, index) => {
                  if(value.key == "basic") {
                    return (
                      <div key={"section-question-"+index}>
                        {value.questions.map((question, qIndex) => {
                          return this._createQuestionView(question,qIndex);
                        })}
                      </div>
                    )
                  }
                })}
            </div>
          )}
        </div>
        {this._getFooter()}
      </div>
    );
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    loadQuestionBank: payload => dispatch(loadQuestionBank(payload)),
    loadFlatBank: payload => dispatch(loadFlatBank(payload)),
    loadQuotation: payload => dispatch(loadQuotation(payload)),
    saveQuotation: payload => dispatch(saveQuotation(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    questionBank: state.questionBank,
    flatBank: state.flatBank,
    quotation: state.quotation
    // availableCurrencies: state.availableCurrencies
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Calculator));


// if (document.getElementById('example')) {
//     ReactDOM.render(<Calculator />, document.getElementById('example'));
// }
