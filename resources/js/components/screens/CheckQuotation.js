import React from 'react';
import ReactDOM from 'react-dom';
import store from '../../store/index';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";


class CheckQuotation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ref:"",
      phone:"",
      isLoading:false,
      errMessage:"",
      isRetrived:false,
      quotation_ref:"",
      quotation_downloadURL:"",
      quotation_modifyURL:"",
    }
  }
  componentDidMount () {

  }

  _onSubmit = () => {
    this.setState({isLoading:true});
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          ref:this.state.ref,
          phone: this.state.phone
        })
    };
    fetch('/api/inquiry-quotation', requestOptions)
        .then(response => response.json())
        .then(data => {
          if(data.status == "fail") {
            this.setState({
              errMessage: data.data.message,
              isLoading:false,
            });
          }
          if(data.status == "success") {
            this.setState({
              isLoading:false,
              isRetrived:true,
              quotation_ref:data.data.quotation_ref,
              quotation_downloadURL:data.data.quotation_downloadURL,
              quotation_modifyURL:data.data.quotation_modifyURL,
            });
            // this.props.submitQuotation({data:data.data})
            // this.props.history.push('/success');
          }
        });
  }


  render() {
    return (
      <div className="container">
          <div className="row justify-content-center">
              <div className="col-md-8">
                <div className="check-quotation">
                  <h2>查看報價</h2>
                  <br/><br/>
                  <div className="form-group">
                    <label>報價單編號：</label>
                    <input className="form-control" value={this.state.ref} onChange={(e)=>{this.setState({ref:event.target.value})}}/>
                  </div>
                  <div className="form-group">
                    <label>電話號碼：</label>
                    <input className="form-control" value={this.state.phone} onChange={(e)=>{this.setState({phone:event.target.value})}}/>
                  </div>
                  <span className="error-message">{this.state.errMessage}</span><br/><br/>
                  <button className="btn btn-primary" onClick={()=>this._onSubmit()}>遞交</button>
                  {this.state.isRetrived == true && (
                    <div className="quotation-content">
                      <br/>
                      <hr/>
                      <p>參考編號: <span>{this.state.quotation_ref}</span></p>
                      <br/>
                      <a href={this.state.quotation_downloadURL}>下載PDF</a>
                      <br/>
                      <br/>
                      <Link to={this.state.quotation_modifyURL} className="btn btn-secondary">修改</Link>
                    </div>
                  )}
                </div>
              </div>
          </div>
      </div>
    );
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    // addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    // changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    // currentCurrency: state.currentCurrency,
    // availableCurrencies: state.availableCurrencies
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckQuotation));

// if (document.getElementById('example')) {
//     ReactDOM.render(<Example />, document.getElementById('example'));
// }
