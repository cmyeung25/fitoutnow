import React from 'react';
import ReactDOM from 'react-dom';
import store from '../../store/index';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

class QuotationSubmitSuccess extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {

  }
  render() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                  <div className="quotation-submit-success">
                    <h2>成功遞交</h2>
                    <p>參考編號: <span>{this.props.quotation_data.quotation_ref}</span></p>
                    <br/>
                    <a href={this.props.quotation_data.quotation_downloadURL}>下載PDF</a>
                  </div>
                </div>
            </div>
        </div>
    );
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    // addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    // changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    // currentCurrency: state.currentCurrency,
    // availableCurrencies: state.availableCurrencies
    quotation_data: state.quotation_data
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuotationSubmitSuccess));

// if (document.getElementById('example')) {
//     ReactDOM.render(<QuotationSubmitSuccess />, document.getElementById('example'));
// }
