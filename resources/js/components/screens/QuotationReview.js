import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { submitQuotation } from '../../actions/index';

class QuotationReview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      regroupAnswser: [],
    };

  }
  componentDidMount () {
    this.setState({regroupAnswser:this._regroupAnswer()})
  }
  _regroupAnswer = () => {
    var ansByCategory = {};
    for (var questionId in this.props.quotation.answers) {
      let answerData = this.props.quotation.answers[questionId];
      let question = answerData.question;
      let category = question.category;

      let roomIndex = typeof answerData.roomIndex == "undefined" ? (typeof answerData.answer == "undefined" ? 0 : answerData.answer.roomIndex) : answerData.roomIndex;
      let categoryId = category.key + "_" + roomIndex;

      if(typeof ansByCategory[categoryId] == "undefined"){
        ansByCategory[categoryId] = {
          category: category,
          roomIndex: roomIndex,
          answers: [],
          groupedAnswers: [],
        }
      }



      // console.log(answerData);
      ansByCategory[categoryId].answers.push(answerData);
    }


    for (var categoryId in ansByCategory) {
      for (var answerId in ansByCategory[categoryId].answers) {
        var answerData = ansByCategory[categoryId].answers[answerId];

        if(!answerData.isMultple) {
          var existingIndex = this._checkIfAnswerExist(ansByCategory[categoryId].groupedAnswers, answerData.answer);
          if(existingIndex != false) {

            // ansByCategory[categoryId].groupedAnswers[existingIndex].display =
            ansByCategory[categoryId].groupedAnswers[existingIndex].answer.unit += parseInt(ansByCategory[categoryId].groupedAnswers[existingIndex].answer.unit) + parseInt(answerData.answer.unit);
            // ansByCategory[categoryId].groupedAnswers[existingIndex].inputUnit =
            ansByCategory[categoryId].groupedAnswers[existingIndex].answer.unitPrice = "混合";
            ansByCategory[categoryId].groupedAnswers[existingIndex].answer.cost += answerData.answer.cost;
          } else {
            // console.log(answerData.answer);
            ansByCategory[categoryId].groupedAnswers.push({
              answer: {
                display: answerData.answer.display,
                unit: answerData.answer.unit,
                answerType: answerData.answer.answerType,
                inputUnit: answerData.answer.inputUnit,
                unitPrice: answerData.answer.unitPrice,
                cost: answerData.answer.cost,
                selectedOption: answerData.answer.selectedOption,
              },
              isMultple: false,
            })
          }
        } else {
          ansByCategory[categoryId].groupedAnswers.push({
            // display: answerData.answers.display,
            // unit: answerData.answers.unit,
            // inputUnit: answerData.answers.inputUnit,
            // unitPrice: answerData.answers.unitPrice,
            // cost: answerData.answers.cost,
            answers: answerData.answers,
            isMultple: true,
          })
        }
      }
    }
    return ansByCategory;
  }

  _checkIfAnswerExist = (groupedAnswers, answer) => {
    for (var groupedAnswerId in groupedAnswers) {
      if(typeof groupedAnswers[groupedAnswerId].answer !="undefined" && groupedAnswers[groupedAnswerId].answer.display == answer.display) {
        return groupedAnswerId;
      }
    }
    return false;
  }


  _submitQuotation = () => {
    this.setState({isLoading:true});
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          quotation:this.props.quotation,
          regroupAnswser: this.state.regroupAnswser
        })
    };
    fetch('/api/save-quotation', requestOptions)
        .then(response => response.json())
        .then(data => {
          if(data.status == "success") {
            this.props.submitQuotation({data:data.data})
            this.props.history.push('/success');
          }
          this.setState({isLoading:false});
        });
  }
  _displayPrice = (content) => {
    if(typeof content == "string" || content == 0) {
      return "-";
    } else {
      return new Intl.NumberFormat().format(content);
    }
  }

  _getFooter = () => {
    var actionText = "";
    var actionFunction = null;
    actionText = <span>遞交 <i className="fa fa-angle-right"></i></span>
    actionFunction = () => this._submitQuotation();

    return (
      <div className="quotation-footer">
        <div className="footer-left">

        </div>
        <div className="footer-right">
          <button onClick={actionFunction} disabled={this.state.isLoading}>
            {actionText}
          </button>
        </div>
      </div>
    )
  }
  render() {
    var ansByCategory = this.state.regroupAnswser;

    var quotation = this.props.quotation;
    var today = new Date();
    var costTest = 0;
    if(quotation.flat!=null) {
      return (
        <div className="container">
          <div className="quotation-review-screen">
            <h2>報價單</h2>
            <div className="quotation-header">
              <div className="row">
                <div className="col-md-6">
                  <div className="quotation-header-attribute">
                    <span>客人:</span> {quotation.contactInfo.name} ({quotation.contactInfo.phone})
                  </div>
                  <div className="quotation-header-attribute">
                    <span>工程:</span> {quotation.flat.description}
                  </div>
                  <div className="quotation-header-attribute">
                    <span>日期:</span> {today.getFullYear()}年{today.getMonth()}月{today.getDay()}日
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="quotation-header-attribute">
                  </div>
                </div>
              </div>
            </div>
            <div className="quotation-body">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>項目</th>
                    <th>內容</th>
                    <th>數量</th>
                    <th>單位</th>
                    <th>單價</th>
                    <th>加總</th>
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(ansByCategory).map((categoryKey,cIndex)=>{
                    var categoryData = ansByCategory[categoryKey]
                    var answers = categoryData.groupedAnswers;
                    // var answers = categoryData.answers;
                    var rowData = [];
                    var nonDisplayedCost = 0;
                    var answerIndex = 1;

                    rowData.push(
                      <tr>
                        <td colSpan="6">{categoryData.category.title}</td>
                      </tr>
                    )
                    for (var answerId in answers) {
                      var answerData = answers[answerId];
                      if(answerData.isMultple == true) {
                        var multiAnswers = [];
                        var display = ""
                        var unit = ""
                        var inputUnit = ""
                        var unitPrice = ""
                        var cost = "";

                        for (var i in answerData.answers) {
                          var existIndex = -1;
                          for (var j in multiAnswers) {
                            if(answerData.answers[i].display == multiAnswers[j].display) {
                              existIndex = j;
                            }
                          }

                          if(existIndex > -1) {
                            multiAnswers[existIndex].unit = parseInt(multiAnswers[existIndex].unit) + parseInt(answerData.answers[i].unit);
                            multiAnswers[existIndex].cost += answerData.answers[i].cost;
                          } else {
                            multiAnswers.push({...answerData.answers[i]});
                          }
                        }
                        for (var i in multiAnswers) {
                          if(typeof multiAnswers[i].display != 'undefined' && multiAnswers[i].display != "") {
                            display += multiAnswers[i].display + "<br>"
                            unit += multiAnswers[i].unit + "<br>"
                            inputUnit += multiAnswers[i].inputUnit + "<br>"
                            unitPrice += "HKD " + this._displayPrice(multiAnswers[i].unitPrice) + "<br/>"
                            cost += "HKD " + this._displayPrice(multiAnswers[i].cost) + "<br/>"
                          } else {
                            nonDisplayedCost += multiAnswers[i].cost;
                          }
                          costTest += multiAnswers[i].cost;
                        }
                        if(display != "") {
                          rowData.push(
                            <tr>
                              <td>{cIndex+1}.{answerIndex}</td>
                              <td><span dangerouslySetInnerHTML={{__html: display}}></span></td>
                              <td><span dangerouslySetInnerHTML={{__html: unit}}></span></td>
                              <td><span dangerouslySetInnerHTML={{__html: inputUnit}}></span></td>
                              <td><span dangerouslySetInnerHTML={{__html: unitPrice}}></span></td>
                              <td style={{textAlign:"right"}}><span dangerouslySetInnerHTML={{__html: cost}}></span></td>
                            </tr>
                          )
                        }
                        answerIndex++;
                      } else {
                        var answer = answerData.answer;
                        var display = answer.display;

                        if(answer.answerType == "select_option") {
                          if(typeof answer.selectedOption != "undefined") {
                            display += "<br> - " + answer.selectedOption.detail.chineseDesc;
                          }
                        }

                        if(typeof display != 'undefined' && display != "") {
                          rowData.push(
                            <tr>
                              <td>{cIndex+1}.{answerIndex}</td>
                              <td><span dangerouslySetInnerHTML={{__html: display}}></span></td>
                              <td>{answer.unit}</td>
                              <td>{answer.inputUnit}</td>
                              <td>HKD {this._displayPrice(answer.unitPrice)}</td>
                              <td style={{textAlign:"right"}}>HKD {this._displayPrice(answer.cost)}</td>
                            </tr>
                          )
                          answerIndex++;
                        } else {
                          nonDisplayedCost += answer.cost;
                        }
                        costTest += answer.cost;
                      }
                    }

                    if(nonDisplayedCost > 0) {
                        rowData.push(
                          <tr>
                            <td>{cIndex+1}.{answerIndex}</td>
                            <td>其他費用</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style={{textAlign:"right"}}>HKD {this._displayPrice(nonDisplayedCost)}</td>
                          </tr>
                        )
                    }

                    return rowData;
                  })}
                </tbody>
                <tfoot>
                  <tr>
                    <td style={{textAlign:"right"}} colSpan="5">共數</td>
                    <td style={{textAlign:"right"}}>HKD {this._displayPrice(quotation.total)}</td>
                  </tr>
                </tfoot>
                {console.log("Total Verification: " + costTest)}
              </table>
            </div>
            {this._getFooter()}
          </div>
        </div>
      );
    } else {
      return null
    }
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    // addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    // changeCurrency: payload => dispatch(changeCurrency(payload)),
    submitQuotation: payload => dispatch(submitQuotation(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    // currentCurrency: state.currentCurrency,
    // availableCurrencies: state.availableCurrencies
    quotation: state.quotation
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuotationReview));

// if (document.getElementById('example')) {
//     ReactDOM.render(<QuotationReview />, document.getElementById('example'));
// }
