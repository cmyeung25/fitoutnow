import React from 'react';
import ReactDOM from 'react-dom';
import store from '../../store/index';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Example extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {

  }
  render() {
    var imgUrl = '/images/banner1.jpg';
    return (
      <div className="home-banner">
        <div className="home-banner-background" style={{ backgroundImage: 'url(' + imgUrl + ')'}}>
        </div>
        <div className="home-content-box">
          <h1>Fitoutnow 專業的團隊致力於為您打造美好生活空間</h1>
          <Link to="/get-started" className="btn btn-primary">馬上報價</Link>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    // addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    // changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    // currentCurrency: state.currentCurrency,
    // availableCurrencies: state.availableCurrencies
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Example);;

// if (document.getElementById('example')) {
//     ReactDOM.render(<Example />, document.getElementById('example'));
// }
