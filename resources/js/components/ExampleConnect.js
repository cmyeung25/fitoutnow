import React from 'react';
import ReactDOM from 'react-dom';
import store from '../store/index';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class Example extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {

  }
  render() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Example Component</div>
                        <div className="card-body">I'm an example component!</div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    // addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    // changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    // currentCurrency: state.currentCurrency,
    // availableCurrencies: state.availableCurrencies
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Example));

// if (document.getElementById('example')) {
//     ReactDOM.render(<Example />, document.getElementById('example'));
// }
