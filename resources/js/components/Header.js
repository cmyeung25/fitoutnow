import React from 'react';
import ReactDOM from 'react-dom';
import store from '../store/index';
import { connect } from "react-redux";
import { Dropdown } from "react-bootstrap";
// import { loadQuestionBank } from '../actions/index';
import { Link } from "react-router-dom";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    // fetch("/api/exchange-rate")
    // .then(res => res.json())
    // .then(
    //   (result) => {
    //     this.props.addExchangeRate(result);
    //   }
    // )
  }
  render() {
    return (
      <nav className="header navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div className="container">
          <Link to="/" className="navbar-brand">Fitoutnow</Link>
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/get-started" className="nav-link">馬上報價</Link>
            </li>
            <li className="nav-item">
              <Link to="/check-quotation" className="nav-link">查看報價</Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
// <Dropdown>
// <Dropdown.Toggle className="nav-curreny-selector">
// {this.props.currentCurrency}
// </Dropdown.Toggle>
// <Dropdown.Menu>
// {
//   this.props.availableCurrencies.map((currency, i) => (
//     (
//       <Dropdown.Item key={"currency-dropdown-" + i} onSelect={this.props.changeCurrency} eventKey={currency}>{currency}</Dropdown.Item>
//     )
//   ))
// }
// </Dropdown.Menu>
// </Dropdown>
const mapDispatchToProps = function (dispatch) {
  return {
    addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    currentCurrency: state.currentCurrency,
    availableCurrencies: state.availableCurrencies
  };
};

export default connect(mapStateToProps, mapDispatchToProps )(Header);
// window.store = store
