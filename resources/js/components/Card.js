import React from 'react';
import ReactDOM from 'react-dom';
import Button from 'react-bootstrap/Button';
function Card(props) {

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Card Component</div>

                        <div className="card-body">
                          <Button variant="primary">Primary</Button>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    );
}

var uiCard = document.getElementsByClassName('ui-card')
if (uiCard) {
  for (var i = 0; i < uiCard.length; i++) {
    ReactDOM.render(<Card/>, uiCard[i]);
  }
}

export default Card;
