import React from 'react';
import ReactDOM from 'react-dom';
import store from '../store/index';

class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // done: false,
      // hasMore: false,
      // elements: [],
      // page: 0,
    };
  }

  store = this.getCurrentStateFromStore();

  getCurrentStateFromStore() {
    return {
      // currentCurrency: store.getState().currentCurrency,
      // availableCurrencies: store.getState().availableCurrencies,
      // exchangeRates: store.getState().exchangeRates
    }
  }

  updateStateFromStore = () => {
    const currentState = this.getCurrentStateFromStore();

    if (this.state !== currentState) {
      this.setState(currentState);
    }
  }

  componentDidMount () {
    this.unsubscribeStore = store.subscribe(this.updateStateFromStore);
  }

  componentWillUnmount() {
    this.unsubscribeStore();
  }

  render() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Example Component</div>
                        <div className="card-body">I'm an example component!</div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
