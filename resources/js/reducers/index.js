import {
  LOAD_QUESTION_BANK,
  LOAD_FLAT_BANK,
  SAVE_QUOTATION,
  LOAD_QUOTATION,
  SUBMIT_QUOTATION,
  CLEAR_QUOTATION,
} from "../constants/action-types";

const getData = JSON.parse(window.getData);
const quotationVersion = 1;

var quotation_stored = localStorage.getItem('quotation');
if(quotation_stored!=null) {
  if(JSON.parse(quotation_stored).version != quotationVersion) {
    quotation_stored = null;
  }
}

var quotation_data_stored = localStorage.getItem('quotation_data');

const initialQuatation = {
  flat: null,
  view: "flat-selection-screen",//flat-selection-screen,
  roomIndex: 0,
  roomContent: null,
  bedroomCount: 0,
  answers:{},
  total:0,
  showQuestions:[],
  contactInfo: {
    title:"",
    name:"",
    phone:"",
    email:""
  },
  options:{
    title:["先生","小姐","太太"],
  }
}

const initialState = {
  questionBank:null,
  flatBank:null,
  quotation_data: quotation_data_stored != null ? JSON.parse(quotation_data_stored) : null,
  quotationExist: quotation_stored != null,
  quotation:quotation_stored != null ? JSON.parse(quotation_stored).content : initialQuatation,
};



function rootReducer(state = initialState, action) {
  if (action.type === LOAD_QUESTION_BANK) {
    state = {...state,questionBank:action.payload.data};
  }
  if (action.type === LOAD_FLAT_BANK) {
    state = {...state,flatBank:action.payload.data};
  }
  if (action.type === SAVE_QUOTATION) {
    var toStore = {
      content: action.payload.data,
      version: quotationVersion,
    }
    localStorage.setItem('quotation', JSON.stringify(toStore))

    state = {...state,quotation:action.payload.data};
  }
  if (action.type === SUBMIT_QUOTATION) {
    // localStorage.removeItem('quotation');
    var quotation_data = {quotation_ref: action.payload.data.quotation_ref,quotation_downloadURL:action.payload.data.quotation_downloadURL};
    localStorage.setItem('quotation_data', JSON.stringify(quotation_data))
    state = {...state,quotation: {...initialQuatation}, quotation_data:quotation_data,quotationExist:false};
  }
  if (action.type === CLEAR_QUOTATION) {
    // localStorage.removeItem('quotation');
    state = {...state,quotation: {...initialQuatation}, quotation_data:null,quotationExist:false};
  }


  return state;
};
export default rootReducer;
