(function($){
  $(document).ready(function(){
    var $containers = $('.unread-comments-count');
    if($containers) {
      $.get(siteUrl + '/api/unread-comments-count').then(function(response){
        var data = JSON.parse(response).data
        if(data > 0) {
          $containers.addClass('has-unread-comments').html(data);
        }
      })
    }
  })
})($);
