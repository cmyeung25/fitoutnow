(function($){
  function handleImage() {
    input = this;
    $this = $(this);

    var reader = new FileReader();
    if(input.files && input.files[0]) {
      reader.onload = function(e){
        $uploader.find('.image-preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
    $uploader.find('.uploader-trigger-button').hide();
    $uploader.find('.delete-image').show();

  }

  function init() {
    if(imageData=="") {
      $uploader.append('\
      <button class="uploader-trigger uploader-trigger-button"><i class="fa fa-picture-o"></i>上載圖片</button>\
      <input type="file" name="' + imageFieldName + '" class="uploader" accept="image/*" >\
      <div class="image-preview-container uploader-trigger">\
        <img class="image-preview" src="" alt="">\
      </div>\
      <a href="#" class="delete-image" style="display:none">移除圖片</a>\
      ')
    } else {
      $uploader.append('\
      <input type="hidden" name="' + imageFieldName + '_data" value="' + imageData + '">\
      <input type="file" name="' + imageFieldName + '" class="uploader" accept="image/*" >\
      <div class="image-preview-container uploader-trigger">\
        <img class="image-preview" src="' + imageData + '" alt="">\
      </div>\
      <a href="#" class="delete-image">移除圖片</a>\
      ')
    }
    $uploader.find('.uploader').last().on('change', handleImage);
  }

  //init load data
  var $uploader = $('.image-uploader');
  if($uploader.length > 0) {
    imageData = $uploader.attr('data-image-data');
    imageFieldName = $uploader.attr('data-image-field-name');
    init();

    $uploader.on('click','.uploader-trigger', function(e){
      e.preventDefault();
      $this = $(this)
      $uploader.find('.uploader').trigger('click');
    })

    $uploader.on('click','.delete-image', function(e){
      e.preventDefault();
      $this = $(this)
      $uploader.html('');
      imageData = '';
      init();
    })

  }






})(jQuery);
