(function($){
  $(document).ready(function(){
    $('.smooth-goto').each(function(index, item){
      $this = $(this)
      $target = $('#' + $this.attr('data-target'));
      $this.on('click', function(){
        $('html, body').animate({
            scrollTop: $target.offset().top 
        }, 500);
      })
    })
  })
})($)
