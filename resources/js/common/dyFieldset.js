(function($){
    $.fn.dyField = function(option){
        this.option = $.extend({
            onInit: null,
        },option);

        var $this = this;

        $this.find('select.select2').select2("destroy");

        var baseFieldSet = $this.find('fieldset');
        var baseFieldSetTemplate = $this.find('fieldset').wrap('<div>').parent().html();;
        $this.find('fieldset').unwrap();

        var triggerButton = $this.find('.dy-field-set-trigger');
        var listContainer = $this.find('.dyfield-list');
        var count = 1;

        triggerButton.on('click', function(e){
            e.preventDefault();
            count++;
            $this.dyFieldSet($(baseFieldSetTemplate));
        })

        this.dyFieldSet = function(base) {
            $this.trigger('dyFieldSet::changeStart', [this, count]);
            newItem = base.clone(true);
            newItem.html(newItem.html().replace(/\%i%/g, count));
            newItem.appendTo(listContainer);
            $this.trigger('dyFieldSet::changeEnd', [this, count]);
        }

        $this.on('click', '.dy-field-remove' , function(e){
            e.preventDefault();
            $button = $(this);
            $button.closest('fieldset').remove();
        })

        baseFieldSet.html(baseFieldSet.html().replace(/\%i%/g, count));

        if(typeof($this.option.onInit) == 'function') {
          $this.option.onInit(baseFieldSet,count);
        }

        if(typeof $this.attr('data-context') != "undefined") {
          var data = $this.attr('data-context');
          var dataJson = JSON.parse(data);
          for (var i = 0; i < dataJson.length; i++) {
            // dataJson[i]
            count++;
            newItem = $(baseFieldSetTemplate).clone(true);
            newItem.html(newItem.html().replace(/\%i%/g, count));
            newItem.appendTo(listContainer);
            $this.option.onInit(baseFieldSet,count,dataJson[i]);
          }
        }

        return this;
    }
})(jQuery);
