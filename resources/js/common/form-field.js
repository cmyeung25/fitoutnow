(function($) {
  $('.select2').select2();
  $('.select2-tag').select2({
    'tags' : true
  });

  $("button, input[name='submit']").on('click', function(e){
    $this = $(this);
    if($this.hasClass('disabled')) {
      e.preventDefault();
    }
  });

})(jQuery);
