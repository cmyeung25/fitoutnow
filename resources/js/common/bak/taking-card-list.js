(function($){
  var initialized = false;
  $.fn.takingCard = function(){

    var $this = this;

    function init() {
      var offset = 60;
      var currentOffset = 0;
      var zIndex = 1;

      $this.find('.taking-card').each(function(index,item){
        $card = $(item);
        $card.css({
          transform: 'translate(0px,' + currentOffset + 'px)',
          'z-index': zIndex,

        })
        currentOffset = currentOffset + offset;
        zIndex = zIndex + 1;

      })
      initialized = true;
    }

    function setInFocusOtherCardsPosition() {

      var collapsedCardOffset = 5;
      var collapsedCardCurrentOffset = 0;
      var zIndex = 1;
      var containerHeight = $(window).height();

      $this.find('.taking-card').each(function(index,item) {
        $otherCard = $(item);
        targetTop = containerHeight - 170 + collapsedCardCurrentOffset;
        $otherCard.removeClass('taking-card-focus');

        $otherCard.css({
          transform: 'translate(0px,' + targetTop + 'px)',
          'z-index': zIndex,
        });

        collapsedCardCurrentOffset = collapsedCardCurrentOffset + collapsedCardOffset;
        zIndex = zIndex + 1;
      })
    }

    function resetCardsPosition(){
      init();
      $this.find('.taking-card').removeClass('taking-card-focus');
      $this.removeClass('taking-card-list-infocus');
    }


    if(!initialized) {
      var containerHeight = $(window).height();

      $this.on('click', '.taking-card', function(e){
        var $card = $(this);

        if($card.hasClass('taking-card-focus'))
          return

        setInFocusOtherCardsPosition();
        targetHeight = containerHeight - 70;
        $card.addClass('taking-card-focus').css({
          transform: 'translate(0px,0px)',
          'z-index': 0,
        });
        $card.find('.taking-card-overflow-content').css({
          height: targetHeight,
        })

        $this.addClass('taking-card-list-infocus');
      })

      $this.on('click','.taking-card-return-holder',function(){
        resetCardsPosition();
      })
      $this.on('click','.taking-card-header',function(e){
        var $header = $(this);
        if ($header.hasClass('taking-card-close-prevent'))
          return;
          
        if($header.closest('.taking-card').hasClass('taking-card-focus')) {
          resetCardsPosition();
          e.stopPropagation();
        }
      })

    }

    $this.init = function(){
      init();
    }

    // init();

    return $this;
  }

  $(document).ready(function () {
    $('.taking-card-list').takingCard();
  })
})(jQuery);
