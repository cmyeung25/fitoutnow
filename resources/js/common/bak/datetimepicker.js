(function($){
  // $("input[type='time']").each(function(){
  //   var $this = $(this);
  //
  //   var time = moment($this.val(), 'HHmm');
  //   console.log($this, time, time.format('HHmm'));
  //
  // })
  // $("input[type='date']").each(function(){
  // });

  // return false;
  //timepicker and datepicker backup
  $("input[type='time']").each(function(){
    var timepicker = $(this).pickatime();
    timepicker.pickatime('on', {
      open: function(){
        var $this = $(this.$node);
        var val = typeof $this.attr('data-value') == 'undefined' ? $this.val() : $this.attr('data-value');

        var picker = this;
        var noon = moment('0:0', 'H:m');
        var time = moment(val, 'HHmm');

        if($this.val() == '') {
          picker.set('view', 600);
          picker.set('highlight', 600);
        } else {
          picker.set('view', time.diff(noon,'m'));
          picker.set('highlight', time.diff(noon,'m'));
        }
      }
    })
  })

  $("input[type='date']").each(function(){
    var datepicker = $(this).pickadate({
      format: "yyyy-mm-dd",
    });

    datepicker.pickadate('on', {
      open: function(){
        var $this = $(this.$node);

        var val = typeof $this.attr('data-value') == 'undefined' ? $this.val() : $this.attr('data-value');

        var picker = this;
        var date = moment(val, 'YYYY-MM-DD').toDate();

        picker.set('view', date);
        picker.set('highlight', date);
      }
    })
  })

})(jQuery);
