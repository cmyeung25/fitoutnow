(function($){
  $('.tooltip-trigger').tooltip();
  $('.tooltip-theme-trigger').tooltip({
    template: '<div class="tooltip theme" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
  });

})(jQuery);
