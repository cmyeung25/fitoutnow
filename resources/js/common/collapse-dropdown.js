(function($){
    $('body').on('click', '.collapse-dropdown-wrapper .collapse-dropdown-toggle', function(e){
        $this = $(this);
        $body = $('body');
        $list = $this.closest('.collapse-dropdown-wrapper').find('.collapse-dropdown-list')
        $list.toggleClass('show');
        $body.toggleClass('collapse-dropdown-list-shown');
        // console.log('toggled');
    })
})(jQuery);
