(function($){
  $('.contact-methods').each(function(index, item){
    var $this = $(item);
    var jsonData = $this.attr('data-json');
    var namePrefix = $this.attr('data-name');
    if(jsonData != "") {
      jsonData = JSON.parse(jsonData);
    }

    for(var key in jsonData) {
      var inputName = namePrefix + '[' + jsonData[key].key + '][value]';

      $this.append('<div>\
          <label class="contact-method-label" for="' + inputName + '">' + jsonData[key].name + '</label>\
          <input class="contact-method-control" type="text" id="' + inputName + '" name="' + inputName + '" value="' + jsonData[key].value + '"/>\
        </div>\
      ')
    }

  })
})(jQuery);
