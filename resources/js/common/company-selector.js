(function($){
  if(!$('body').hasClass('member'))
    return

    
  $('.company-selector').on('change', function() {
    $this = $(this)
    var selectValue = $this.find(':selected').val();
    var params = parseQueryString(window.location.search);
    var currentUrl = window.location.href.split("?")[0];
    params.company = selectValue;
    var targetUrl = currentUrl + "?" + decodeURIComponent($.param(params));

    window.location.href = targetUrl;
  });

  $('a').each(function(i,e){
    $this = $(this);
    var url = $this.attr('href');
    if(typeof url != 'undefined') {
      url = url.split("?");

      var params = {};
      var selectValue = getParameterByName('company')
      if(typeof url[1] != 'undefined') {
        var params = parseQueryString('?' + $this.attr('href').split("?")[1]);
      }
      var currentUrl = $this.attr('href').split("?")[0];
      if(selectValue != null) {
        params.company = selectValue;
      }
      var targetUrl = currentUrl + "?" + decodeURIComponent($.param(params));

      $this.attr('href', targetUrl);
    }
  })

  $('form').each(function(i,e){
    $this = $(this);
    var url = $this.attr('action');
    if(typeof url != 'undefined') {
      url = url.split("?");

      var params = {};
      var selectValue = getParameterByName('company')
      if(typeof url[1] != 'undefined') {
        var params = parseQueryString('?' + $this.attr('action').split("?")[1]);
      }
      var currentUrl = $this.attr('action').split("?")[0];
      if(selectValue != null) {
        params.company = selectValue;
      }
      var targetUrl = currentUrl + "?" + decodeURIComponent($.param(params));

      $this.attr('action', targetUrl);
    }
  })
  //utils
  function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  function parseQueryString(queryString) {
    var params = {}, queries, temp, i, l;
    queryString = queryString.replace('?','')
    // Split into key/value pairs
    queries = queryString.split("&");
    // Convert the array of strings into an object
    for ( i = 0, l = queries.length; i < l; i++ ) {
        temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
    return params;
  };

})(jQuery);
