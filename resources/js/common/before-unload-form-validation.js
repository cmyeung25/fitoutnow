(function($){
  var checkUnloadIgnore = false;

  $('form.check-unload').each(function(index, item){
    var $this = $(item);
    var initData = "";

    function fetchFormData() {
      return $this.serialize();
    }

    //ensure all js plugin is loaded
    setTimeout(function(){
      initData = fetchFormData();
      window.onbeforeunload = function(e) {
        if(initData != fetchFormData() && !checkUnloadIgnore) {
          return confirm()
        }
      }
    },1000);

    $this.on('submit', function(e){
      checkUnloadIgnore = true;
    })

  })
})(jQuery);
