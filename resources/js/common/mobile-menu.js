(function($){
    var $body = $('body');
    var $trigger = $('.mobile-menu-trigger');
    var bodyShowClass = 'body-menu-show';
    var menuShowClass = 'menu-show';
    var $wrapper = $('body > .wrapper');
    $trigger.on('click', function(){
      var $trigger = $(this);
      var $targetMenu = $($trigger.data('toggle-menu'));


      var mobileMenuIsActive = $body.hasClass(bodyShowClass);

      if(!mobileMenuIsActive) {
        showMenu()
      } else {
        hideMenu()
      }

      function showMenu() {
        $body.addClass(bodyShowClass);
        $targetMenu.addClass(menuShowClass);
        setTimeout(function(){
          // $('.banner-swiper').trigger('resize');
          $wrapper.on('click', function(){
            hideMenu();
          })
        },500)
      }

      function hideMenu(){
        $body.removeClass(bodyShowClass);
        $targetMenu.removeClass(menuShowClass);
        $wrapper.unbind('click');
      }
    })
})(jQuery);
