(function($){
  $('.tree-checkbox-container > ul').each(function(){
    $this = $(this);

    $this.on('click','input[type="checkbox"]', function(e) {
      $checkbox = $(this);
      calcChildren($checkbox);
      calcParents($checkbox);
    });

    function calcCheckBox($list) {
      $list.find('input[type="checkbox"]').each(function(){
        this.checked = true;
      })
    }

    function calcParents($checkbox) {
      if($checkbox.get(0).checked) {
        //check slibing
        var allChecked = true;
        $checkbox.closest('ul').find('input[type="checkbox"]').each(function(){
          if(!this.checked) {
            allChecked = false;
          }
        });

        if(allChecked){
          $parent = $checkbox.closest('ul').closest('li').find('> label > input[type="checkbox"]');
          if (typeof $parent.get(0) != "undefined") {
            $parent.get(0).checked = true;
          }

          var parentAllChecked = true;
          $parent.closest('ul').find('input[type="checkbox"]').each(function(){
            if(!this.checked) {
              parentAllChecked = false;
            }
          });

          if(parentAllChecked) {
            $grandParent = $parent.closest('ul').closest('li').find('> label > input[type="checkbox"]');
            if (typeof $grandParent.get(0) != "undefined") {
              $grandParent.get(0).checked = true;
            }
          }

        }

      } else {
        //uncheck parents
        uncheckParent($checkbox)
      }
    }

    function uncheckParent($checkbox) {
      $parentCheckbox = $checkbox.closest('ul').closest('li').find('input[type="checkbox"]');
      if(typeof $parentCheckbox.get(0) != 'undefined') {
        $parentCheckbox.get(0).checked = false;
      }

      $grandParentCheckbox = $parentCheckbox.closest('ul').closest('li').find('> label > input[type="checkbox"]');

      if(typeof $grandParentCheckbox.get(0) != 'undefined') {
        $grandParentCheckbox.get(0).checked = false;
      }

    }

    function calcChildren($checkbox) {
      if($checkbox.get(0).checked) {
        //check all children
        $checkbox.closest('li').find('input[type="checkbox"]').each(function(){
          this.checked = true;
        })
      } else {
        //uncheck all children
        $checkbox.closest('li').find('input[type="checkbox"]').each(function(){
          this.checked = false;
        })
      }
    }
  })
})(jQuery);
