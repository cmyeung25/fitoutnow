(function($){
  var counter = 0;
  var captionPlaceholder = '';
  function handleImage() {
    input = this;
    $this = $(this);
    $closestLi = $this.closest('li');
    var reader = new FileReader();
    if(input.files && input.files[0]) {
      reader.onload = function(e){
        $closestLi.find('.image-preview').attr('src', e.target.result);
        // $closestLi.find('.image-data').val(e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
    $closestLi.find('.uploader-trigger-button').hide();
    $closestLi.find('.image-caption').show();
  }

  function addImage(data) {
    if(data != null) {
      var domData = '\
      <li>\
        <button class="uploader-trigger uploader-trigger-button" style="display:none;"><i class="fa fa-picture-o"></i>上載圖片</button>\
        <input type="file" name="image_files[' + counter + ']" class="uploader" accept="image/*" >\
        <div class="image-preview-container uploader-trigger">\
          <img class="image-preview" src="' + data['image_url'] + '" alt="">\
        </div>\
        <input class="image-caption" type="text" name="images[' + counter + '][caption]"  value="' + data['caption'] + '" placeholder="' + captionPlaceholder + '">\
        <label>\
          <input type="radio" name="image_thumbnail_index" value="' + counter + '"';
      if(data['main_image'] == 1) {
        domData = domData + 'checked';
      }
      domData = domData + '/>\
          <span>' + isThumbnailTitle + '</span>\
        </label>\
        <input type="hidden" name="images[' + counter + '][image_url]"  value="' + data['image_url'] + '">\
        <a href="#" class="delete-image"><i class="fa fa-times"></i></a>\
      </li>\
      ';
      $('.images-uploader ul').append(domData);
    } else {
      $('.images-uploader ul').append('\
      <li>\
        <button class="uploader-trigger uploader-trigger-button"><i class="fa fa-picture-o"></i>上載圖片</button>\
        <input type="file" name="image_files[' + counter + ']" class="uploader" accept="image/*" >\
        <div class="image-preview-container uploader-trigger">\
          <img class="image-preview" src="" alt="">\
        </div>\
        <input class="image-caption" type="text" name="images[' + counter + '][caption]" style="display:none;" value="" placeholder="' + captionPlaceholder + '">\
        <label>\
          <input type="radio" name="image_thumbnail_index" value="' + counter + '"/>\
          <span>' + isThumbnailTitle + '</span>\
        </label>\
        <a href="#" class="delete-image"><i class="fa fa-times"></i></a>\
      </li>\
      ');
    }


    counter++;
    $('.images-uploader .uploader').last().on('change', handleImage);
  };

  $('.images-uploader').on('click','.uploader-trigger', function(e){
    e.preventDefault();
    $this = $(this)
    $this.closest('li').find('.uploader').trigger('click');
  })

  $('.images-uploader').on('click', '.delete-image',function(e){
    e.preventDefault();
    $this = $(this);
    $this.closest('li').remove();
  });

  $('.images-uploader .add-image').on('click', function(){
    addImage(null);
  });

  //init load data
  var $uploader = $('.images-uploader');
  if($uploader.length > 0) {
    imageData = JSON.parse($uploader.attr('data-images-data'));
    captionPlaceholder = $uploader.attr('data-caption-placeholder');
    isThumbnailTitle = $uploader.attr('data-thumbnail-title');

    if(imageData.length > 0) {
      for (var i = 0; i < imageData.length; i++) {
        addImage(imageData[i]);
      }
    } else {
      addImage(null);
    }
  }



})(jQuery);
