(function($){
 $('.confirm-required').on('click', function(e) {
   $this = $(this);
   var response = confirm($this.attr('confirm-message'));

   if(!response) {
     return e.preventDefault();
   }
 })
})(jQuery);
