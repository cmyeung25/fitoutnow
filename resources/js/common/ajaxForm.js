console.log('ajaxForm loaded');
$('.ajaxForm').ajaxForm({
  beforeSubmit: function(arr, $form, options) {
    $form.find("button, input[name='submit']").addClass('disabled');
    $form.find('.message').html('');
    $form.find('.form-group').removeClass('error').find('.error-message').html('');
  },
  complete: function(responseText, xhr, $form) {
    $form.find("button, input[name='submit']").removeClass('disabled');

    if(responseText.status == 500) {
      var errorMessage = 'Error Occurred. Please refresh to try again.';
      if($form.find('.message').length > 0) {
        $form.find('.message').html(errorMessage);
        return false;
      } else {
        return alert(errorMessage);
      }
    }

    var response = JSON.parse(responseText.responseText);
    switch (response.status) {
      case 'fail':
        if( typeof response.data.validator != 'undefined') {
          $.each(response.data.validator, function(key, item) {
            $form.find('[name='+ key +']').closest('.form-group').addClass('error').find('.error-message').html(item);
          })
        }

        if( typeof response.data.message != 'undefined') {
          $form.find('.message').html(response.data.message);
        }

        if( typeof response.data.callback != 'undefined') {
          fn = window[response.data.callback];
          if(typeof fn === 'function') {
            fn(response);
          }
        }
        break;
      case 'success':
        if ( typeof response.data.redirectUrl != 'undefined') {
          window.location.href = response.data.redirectUrl;
        }

        if( typeof response.data.message != 'undefined') {
          $form.find('.message').html(response.data.message);
        }


        if ( typeof response.data.action != 'undefined') {
          switch (response.data.action) {
            case 'clearForm':
              $form.find('textarea').val('') ;
              $form.find('input').val('') ;
              break;
            default:
          }
        }

        if ( typeof response.data.csrf != 'undefined') {
          var csrf = response.data.csrf;
          $form.find('[name="_token"]').val(csrf);
        }
      default:
    }
  }
});
