var appRepo = angular.module('App.Repositories', ['ui-notification']);
var appModel = angular.module('App.Model', ['ngModel']);

var ngApp = angular.module("App",['ngSanitize','ui.select','App.Repositories', 'ngMap', 'ngSelect', 'ngModel']);

ngApp.factory('commonFn',['$rootScope', function($rootScope){
  $rootScope.$viewRefresh = function() {
    if($rootScope.$$phase == null) {
        $rootScope.$apply();
    }
  }
  return true;
}])


appModel.filter('nl2br', ['$sce', function($sce){
    return function(msg,is_xhtml) {
        var is_xhtml = is_xhtml || true;
        var breakTag = (is_xhtml) ? '<br />' : '<br>';
        var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
        return $sce.trustAsHtml(msg);
    }
}]);

appRepo.factory('commonRepo',['$rootScope','Notification', function($rootScope, Notification){
    return {
    setSystemMessages: function(type, message){
      switch (type) {
        case 'success':
          Notification.success(message);
          break;
        case 'error':
          Notification.error(message);
          break;
        default:
          Notification(message);
      }
    },
    getGooglePhotoUrl: function(photoUrl, option) {
      if(photoUrl.indexOf('https://maps.googleapis.com/maps/api/place/photo') > -1) {
        //new vserion from server
        if(typeof option != 'undefined') {
          if(typeof option.width != 'undefined') {
            photoUrl = photoUrl.replace('maxwidth=1600', 'maxwidth=' + option.width);
          }
          if(typeof option.height != 'undefined') {
            photoUrl = photoUrl.replace('maxheight=1600', 'maxheight=' + option.height);
          }
        }
      } else {
        //old version from client
        var photoUrl = photoUrl.split('/');
        photoUrl.pop();
        photoUrl.pop();
        photoUrl = photoUrl.join('/');

        if(typeof option != 'undefined') {
          photoUrl = photoUrl + '/w' + option.width + '-h' +option.height + '-k/';
        }
      }

      return photoUrl;
    }
  }
}]);
