import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import Header from './components/Header'
import InitCalculator from './components/screens/InitCalculator'
import Calculator from './components/screens/Calculator'
import CheckQuotation from './components/screens/CheckQuotation'
import Home from './components/screens/Home'
import QuotationReview from './components/screens/QuotationReview'
import QuotationSubmitSuccess from './components/screens/QuotationSubmitSuccess'

import store from "./store/index";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

render(
  <Provider store={store}>
    <Router>
      <Header />
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>

        <Route exact path="/check-quotation">
          <CheckQuotation/>
        </Route>
        <Route exact path="/get-started">
          <InitCalculator/>
        </Route>
        <Route exact path="/calculator">
          <Calculator/>
        </Route>
        <Route exact path="/modify">
          <Calculator/>
        </Route>
        <Route exact path="/review">
          <QuotationReview/>
        </Route>
        <Route exact path="/success">
          <QuotationSubmitSuccess/>
        </Route>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
)
